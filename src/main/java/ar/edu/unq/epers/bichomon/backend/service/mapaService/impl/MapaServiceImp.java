package ar.edu.unq.epers.bichomon.backend.service.mapaService.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.Session;
import org.hibernate.query.Query;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoArribo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionIncorrectaException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionMuyLejanaException;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.MapaService;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCantEntrenadores;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.ProveedorCostos;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class MapaServiceImp implements MapaService{

	DataHibernateDao dao;;
	UbicacionDao daoU;
	EventoDAO daoEvento;
	ProveedorCostos proveedor;
	CacheCantEntrenadores cache;
	
	public MapaServiceImp(DataHibernateDao dao, UbicacionDao daoU, EventoDAO daoEvento,CacheCantEntrenadores cache){
		this.dao = dao;
		this.daoU = daoU;
		this.daoEvento = daoEvento;
		this.proveedor = new ProveedorCostos();
		this.cache = cache;
	}
	
	public void crearUbicacion(Ubicacion ubicacion) {
		this.daoU.crearUbicacion(ubicacion);
		Runner.runInSession(() -> {
			this.dao.guardar(ubicacion);
			return null;
		});			
	}
	
	@Override
	public void mover(String entrenador, String ubicacion) {
		Runner.runInSession(() -> {
			Entrenador elEntrenador = this.dao.recuperarPorColumna(Entrenador.class, entrenador, "nombre");
			Ubicacion nuevaUbicacion = this.dao.recuperarPorColumna(Ubicacion.class, ubicacion, "nombreUbicacion");
			puedeMover(elEntrenador.getUbicacionActual().getIdUbicacion(), nuevaUbicacion.getIdUbicacion());
			List<List<String>> rutas = this.daoU.relacionesEntreMedio(elEntrenador.getUbicacionActual(), nuevaUbicacion);
			
			List<String> rutaMasBarata = this.proveedor.getRutaMenorCosto(rutas);
			int costo = this.proveedor.getCostoTotal(rutaMasBarata);
			int distancia = rutaMasBarata.size();
			if(distancia > 1){
				throw new UbicacionMuyLejanaException("No podes ir para alla papa");
			}
			Ubicacion uAnterior = elEntrenador.getUbicacionActual();
			elEntrenador.mover(costo, nuevaUbicacion);
			actualizarCache(uAnterior ,elEntrenador);
			dao.update(elEntrenador);
			registrarEvento(elEntrenador.getNombre(), ubicacion);
			return null;	
		});	
	}
	
	private void actualizarCache(Ubicacion ubicacionAnterior, Entrenador elEntrenador ){
		Entrenador entrenador =  new Entrenador(elEntrenador.getNombre());
		entrenador.setId(elEntrenador.getId());
		List<Entrenador> entNuevaUbicacion = cache.get(elEntrenador.getUbicacionActual().getNombreUbicacion());
		List<Entrenador> entUbicacionAnterior = cache.get(ubicacionAnterior.getNombreUbicacion());
		if(entNuevaUbicacion == null){
			cache.put(elEntrenador.getUbicacionActual(), Arrays.asList(entrenador));
		}else{	
				entNuevaUbicacion.add(entrenador);
				cache.put(elEntrenador.getUbicacionActual(),entNuevaUbicacion);
				
			}
		if(entUbicacionAnterior != null){
			cache.put(ubicacionAnterior, this.sacarEntrenadorDeLaUbicacionAnterior(entUbicacionAnterior, elEntrenador));
		}	
	}
	
	private List<Entrenador> sacarEntrenadorDeLaUbicacionAnterior(List<Entrenador> entrenadores , Entrenador ent){
			String nombre = ent.getNombre();
			List<Entrenador> res = entrenadores.stream().filter(e -> !nombre.equals(e.getNombre()))     
																	.collect(Collectors.toList()); 
			return res;
	}
	private void puedeMover(int idUbicacionActual, int idNuevaUbicacion) {
		if(idUbicacionActual == idNuevaUbicacion){
			throw new UbicacionIncorrectaException("Ya estas en esa ubicación");
		}
		
	}

	@Override
	public void moverMasCorto(String entrenador, String ubicacion) {
		Runner.runInSession(() -> {
			Entrenador elEntrenador = this.dao.recuperarPorColumna(Entrenador.class, entrenador, "nombre");
			Ubicacion nuevaUbicacion = this.dao.recuperarPorColumna(Ubicacion.class, ubicacion, "nombreUbicacion");
			puedeMover(elEntrenador.getUbicacionActual().getIdUbicacion(), nuevaUbicacion.getIdUbicacion());
			List<String> tiposCamino = this.daoU.tiposPathMasCorto(elEntrenador.getUbicacionActual(), nuevaUbicacion);			
			int costo = this.proveedor.getCostoTotal(tiposCamino);
			Ubicacion uAnterior = elEntrenador.getUbicacionActual();
			elEntrenador.mover(costo, nuevaUbicacion);
			actualizarCache(uAnterior ,elEntrenador);
			dao.update(elEntrenador);	
			registrarEvento(elEntrenador.getNombre(), ubicacion);
			return null;	
		});	
	}

	private void registrarEvento(String nombreEntrenador, String ubicacion) {
		this.daoEvento.save(new EventoArribo(nombreEntrenador, ubicacion));
		
	}

	@Override
	public int cantidadEntrenadores(String ubicacion) {
		
		
		List<Entrenador> cant =  this.cache.get(ubicacion);
		if(cant == null){
			return Runner.runInSession(() -> {
				Ubicacion ub = this.dao.recuperarPorColumna(Ubicacion.class, ubicacion, "nombreUbicacion");
				List<Entrenador> entrenadores = (List<Entrenador>) ub.getEntrenadoresEnElLugar().stream().
						map(temp ->{
							Entrenador ent = new Entrenador(temp.getNombre());
							ent.setId(temp.getId());
							return ent;
						}).collect(Collectors.toList());
				
				cache.put(ub, entrenadores); 
				return ub.cantidadDeEntrenadores();
			});
		}		
		return cant.size();
	}

	@Override
	public Bicho campeon(String dojo) {
		return Runner.runInSession(() -> {
			Dojo elDojo = this.dao.recuperarPorColumna(Dojo.class, dojo, "nombreUbicacion");
			return elDojo.getBichoCampeon(); 
		});
	}

	@Override
	public Bicho campeonHistorico(String dojo) {
		return Runner.runInSession(() -> {
			String hql = "select bicho "
					+"from Dojo d "
					+ "join d.historialCampeones as c "
					+ "join c.bichoCampeon as bicho "
					+ "group by bicho "
					+ "order by "
					+ "sum(case when c.fechaFin = null then (now() - c.fechaInicio)"
					+ 		" else (c.fechaFin - c.fechaInicio) END) desc";
			Session session = Runner.getCurrentSession();
			Query<Bicho> query = session.createQuery(hql,  Bicho.class);
			query.setMaxResults(1);
			return query.getSingleResult();
		});
	}

	public void conectar(String ubicacion1, String ubicacion2, String tipoConeccion) {
		this.daoU.conectar(ubicacion1, ubicacion2, tipoConeccion);
	}

	public List<Ubicacion> conectados(String ubicacion1, String tipoConeccion) {
		Stream<Map.Entry<String, String>> nombres = this.daoU.conectados(ubicacion1, tipoConeccion).entrySet().stream();
		Stream<Ubicacion> ubicaciones = nombres.map( u ->recuperarPorColumna(Ubicacion.class, u.getKey(), "nombreUbicacion"));
		return ubicaciones.collect(Collectors.toList());
	}
	
	private Class<?> getClass(String tipo){
		try {
			return Class.forName(tipo); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public <T> T recuperarPorColumna(Class<T> tipo, Serializable valor, String columna) {
		return Runner.runInSession(() -> {	
			return this.dao.recuperarPorColumna(tipo, valor, columna);
		});
	}
}
