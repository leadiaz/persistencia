package ar.edu.unq.epers.bichomon.backend.service.bichoService.imp;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Experiencia;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Nivel;
import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoAbandono;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCaptura;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacion;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacionDefault;
import ar.edu.unq.epers.bichomon.backend.model.especie.NoPuedeEvolucionarException;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Resultado;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.BichoService;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;

public class BichoServiceImp implements BichoService{

	DataHibernateDao dao;
	Nivel nivel;
	Experiencia tablaExperiencia;
	EspecieServiceImp especieService;
	FactorBusqueda factorBusqueda;
	EventoDAO daoEvento;
	CacheCampeones cache;
	
	
	public BichoServiceImp(DataHibernateDao dao, EventoDAO daoEvento,CacheCampeones cache ){
		this.dao = dao;
		this.especieService = new EspecieServiceImp(dao);
		this.daoEvento = daoEvento;
		this.cache = cache;
		Runner.runInSession(() -> {
			this.nivel = dao.recuperar(Nivel.class, 1);
			this.tablaExperiencia = dao.recuperar(Experiencia.class, 1);
			return null;
		});
		
	}
	public void SetFactorBusqueda(FactorBusqueda busqueda){
		this.factorBusqueda = busqueda;
	}
	
	@Override
	public Bicho buscar(String nombreEntrenador) {
		return Runner.runInSession(() -> {
			Entrenador entrenador = this.dao.recuperarPorColumna(Entrenador.class, nombreEntrenador, "nombre");	
			if(this.factorBusqueda.busquedaExitosa()){	
				Bicho bicho;
				bicho = entrenador.getUbicacionActual().buscarEnLugar(entrenador);			
				entrenador.agregarBicho(bicho);			
				entrenador.actualizarNivel(tablaExperiencia.getCapturar(), this.nivel);
				this.dao.update(entrenador); 
				registrarEvento(new EventoCaptura(entrenador.getNombre(), 
				entrenador.getUbicacionActual().getNombreUbicacion(), bicho.getEspecie().getNombre()));
				return bicho;
			}
			else{
				throw new BusquedaFallidaException("La busqueda no fué exitosa");
			}				
		});		
	}

	private void registrarEvento(Evento evento) {
		this.daoEvento.save(evento);		
	}

	public void setNivel(Nivel nivel){
		this.nivel = nivel;
	}
	
	@Override
	public void abandonar(String nombreEntrenador, int idBicho){
		Runner.runInSession(() -> {
			Entrenador entrenador = this.dao.recuperarPorColumna(Entrenador.class, nombreEntrenador, "nombre");
			Bicho bicho = entrenador.getBicho(idBicho);
			entrenador.abandonar(bicho);
			this.dao.update(entrenador);
			registrarEvento(new EventoAbandono(entrenador.getNombre(), 
					entrenador.getUbicacionActual().getNombreUbicacion(), bicho.getEspecie().getNombre()));
			return null;
		});			
	}

	@Override
	public Resultado duelo(String nombreEntrenador, int idBicho) {	
		return Runner.runInSession(() -> {
			Entrenador entrenador = this.dao.recuperarPorColumna(Entrenador.class, nombreEntrenador, "nombre");
			Bicho bicho = entrenador.getBicho(idBicho);
			
			Bicho bichoDojo = ((Dojo)entrenador.getUbicacionActual()).getBichoCampeon();
			
			Resultado resultado = entrenador.getUbicacionActual().retar(bicho);
			registrarEventoSiPuede(entrenador.getNombre(), bichoDojo, bicho);
			entrenador.actualizarNivel(tablaExperiencia.getCombatir(), this.nivel);
			Dojo dojoActual = (Dojo)entrenador.getUbicacionActual();
			
			actualizarCacheCampeones(dojoActual, dojoActual.getBichoCampeon().getOwner());
			this.dao.update(entrenador);
			return resultado;
		});
	}

	private void actualizarCacheCampeones(Dojo dojoActual, Entrenador owner) {
		Entrenador entrenador = new Entrenador(owner.getNombre());
		entrenador.setId(owner.getId());
		cache.put(dojoActual, entrenador);		
	}

	private void registrarEventoSiPuede(String entrenador, Bicho bichoDojo, Bicho bicho) {
		if(bichoDojo == null){
			registrarEvento(new EventoCoronacionDefault(entrenador, bicho.getOwner().getUbicacionActual().getNombreUbicacion()));
		}
		else{
			if(bicho.getId() != bichoDojo.getId()){
				registrarEvento(new EventoCoronacion(entrenador, bicho.getOwner().getUbicacionActual().getNombreUbicacion(), 
						bichoDojo.getOwner().getNombre()));
			}
		}
	}

	@Override
	public boolean puedeEvolucionar(String nombreEntrenador, int bicho) {
		return Runner.runInSession(() -> {
			Entrenador elEntrenador = this.dao.recuperarPorColumna(Entrenador.class, nombreEntrenador, "nombre");
			Bicho elBicho = elEntrenador.getBicho(bicho);
			boolean res = elBicho.puedeEvolucionar();//(elBicho, elBicho.getTablaEvoluciones()); 
			return res;
		});	
	}

	@Override
	public Bicho evolucionar(String entrenador, int bicho) {
		return Runner.runInSession(() -> {
			Entrenador elEntrenador = this.dao.recuperarPorColumna(Entrenador.class, entrenador, "nombre");
			Bicho elBicho = elEntrenador.getBicho(bicho);
			if(elBicho.puedeEvolucionar()){
				elEntrenador.evolucionar(elBicho);
				elEntrenador.actualizarNivel(tablaExperiencia.getEvolucionarBicho(), this.nivel);
			}else { throw new NoPuedeEvolucionarException("Este bicho no puede evolucionar");}
			this.dao.update(elBicho);
			return elBicho;
		});	
	}	
}