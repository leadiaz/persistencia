package ar.edu.unq.epers.bichomon.backend.model.entrenador;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Experiencia {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer combatir;
	private Integer capturar;
	private Integer evolucionarBicho;
	
	public Experiencia(){
	}
	
	public Experiencia(Integer combatir, Integer capturar, Integer evolucionar){
		this.combatir = combatir;
		this.capturar = capturar;
		this.evolucionarBicho = evolucionar;
	}
	
	public Integer getCombatir() {
		return combatir;
	}

	public void setCombatir(Integer combatir) {
		this.combatir = combatir;
	}

	public Integer getCapturar() {
		return capturar;
	}

	public void setCapturar(Integer capturar) {
		this.capturar = capturar;
	}

	public Integer getEvolucionarBicho() {
		return evolucionarBicho;
	}

	public void setEvolucionarBicho(Integer evolucionarBicho) {
		this.evolucionarBicho = evolucionarBicho;
	}
}
