package ar.edu.unq.epers.bichomon.backend.model.evento;

public class EventoArribo extends Evento {
	
	protected EventoArribo(){ 
		super();
	}
	
	public EventoArribo( String nombreEnt, String nombreUbi) {
		super(nombreEnt, nombreUbi);
	}	
}
