package ar.edu.unq.epers.bichomon.backend.service.data.impl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Experiencia;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Nivel;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.Evoluciones;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.service.data.DataService;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;

public class DataServiceImp implements DataService{
	
	private DataHibernateDao dao;

	public DataServiceImp(DataHibernateDao dao){
		this.dao = dao;
	}

	public void guardar(Object object) {
		Runner.runInSession(() -> {
			this.dao.guardar(object);
			return null;
		});
	}

	public void update(Object object) {
		Runner.runInSession(() -> {
			this.dao.update(object);
			return null;
		});
	}

	public <T> T recuperar(Class<T> tipo, Serializable key) {
		return Runner.runInSession(() -> {
			return this.dao.recuperar(tipo, key);			 
		});
	}

	@Override
	public void eliminarDatos() {
		this.dao.eliminarDatos();

	}

	public <T> T recuperarPorColumna(Class<T> tipo, Serializable valor, String columna) {
		return Runner.runInSession(() -> {	
			return this.dao.recuperarPorColumna(tipo, valor, columna);
		});
	}


	@Override
	public void crearSetDatosIniciales() {
		List<Integer> limites = new ArrayList<Integer>(Arrays.asList(99, 400, 1000, 2000, 3000, 4000, 5000, 6000,7000));
		Nivel nivel = new Nivel(limites); 
		Experiencia experiencia = new Experiencia(10, 10, 10);	
		Pueblo pueblo = new Pueblo("paleta");
		Dojo dojo = new Dojo("Cobra");
		Entrenador entrenador = new Entrenador("Juan");
		entrenador.setUbicacionActual(pueblo);
		LocalDateTime edad1 = LocalDateTime.of(2017, 1, 20, 4, 2);
		Evoluciones tabla1 = new Evoluciones(0, 0, edad1, 0);
		Evoluciones tabla2 = new Evoluciones(0, 0, null, 4);
		Especie newchu = new Especie("newchu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		Especie raichu = new Especie("raichu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);		
		Especie pikachu = new Especie("pikachu", raichu, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);		
		raichu.setRaiz(pikachu);
		raichu.setEvolucion(newchu);
		newchu.setRaiz(pikachu);
		pikachu.setTablaEvoluciones(tabla1);
		raichu.setTablaEvoluciones(tabla2);

		Runner.runInSession(() -> {
			this.dao.guardar(nivel);
			this.dao.guardar(experiencia);
			this.dao.guardar(pueblo);
			this.dao.guardar(dojo);
			
			this.dao.guardar(tabla1);
			this.dao.guardar(tabla2);
			this.dao.guardar(pikachu);
			this.dao.guardar(newchu);
			this.dao.guardar(raichu);
			return null;
		});
	}
}
