package ar.edu.unq.epers.bichomon.backend.service.evento;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;

public class EventoDAO extends GenericMongoDAO<Evento>{
	
	public EventoDAO() {
		super(Evento.class);
	}
	
	public List<Evento> getByNombreEntrenador(String nombreEnt) {
		return this.findSort("{ nombreEntrenador: # }", "{fechaInicioEvento: -1}", nombreEnt);
	}
	
	
	public List<Evento> getByNombreUbicacion(String nombreUbi) {
		return this.findSort("{ nombreUbicacion: # }", "{fechaInicioEvento: -1}", nombreUbi);
	}
	
	public List<Evento> getByNombresUbicaciones(List<String> nombres) {
		return this.findSort("{ nombreUbicacion: { $in: # } }", "{fechaInicioEvento: -1}", nombres);
	}
}
