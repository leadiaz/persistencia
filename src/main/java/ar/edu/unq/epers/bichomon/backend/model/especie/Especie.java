package ar.edu.unq.epers.bichomon.backend.model.especie;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
/**
 * Representa una {@link Especie} de bicho.
 * 
 * @author Charly Backend
 */
@Entity
public class Especie {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEspecie;
	private String nombre;
	private int altura;
	private int peso;
	@OneToOne(cascade = CascadeType.ALL)
	private Especie evolucion;
	@ManyToOne
	private Especie raiz;
	private int energiaInicial;
	private String urlFoto;
	private int cantidadBichos;
	@OneToOne
	@JoinColumn(name="idEspecieTabla")
	private Evoluciones tablaEvol;
	private TipoBicho tipo;
	
	public Especie()
	{
	}
	
	public Especie(String nombre, Especie evolucion, Especie raiz, int peso, int altura, String foto, TipoBicho tipo,
			int energia, int bichos) 
	{
		this.nombre = nombre;
		this.altura = altura;
		this.peso = peso;
		this.cantidadBichos = bichos;
		this.evolucion = evolucion;
		this.raiz = raiz;
		this.urlFoto = foto;
		this.energiaInicial = energia;
		this.tipo = tipo;
	}
	
	public Especie(String nombre, TipoBicho tipo) 
	{
		this.nombre = nombre;
		this.tipo = tipo;
	}
	
	public void setTablaEvoluciones(Evoluciones tabla){
		this.tablaEvol = tabla;
	}
	
	/**
	 * @return el nombre de la especie (por ejemplo: Perromon)
	 */
	public String getNombre() 
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	
	/**
	 * @return la altura de todos los bichos de esta especie
	 */
	public int getAltura() 
	{
		return this.altura;
	}
	
	public void setAltura(int altura) 
	{
		this.altura = altura;
	}
	
	/**
	 * @return el peso de todos los bichos de esta especie
	 */
	public int getPeso() 
	{
		return this.peso;
	}
	
	public void setPeso(int peso) 
	{
		this.peso = peso;
	}
	
	/**
	 * @return una url que apunta al un recurso imagen el cual será
	 * utilizado para mostrar un thumbnail del bichomon por el frontend.
	 */
	public String getUrlFoto() 
	{
		return this.urlFoto;
	}
	
	public void setUrlFoto(String urlFoto) 
	{
		this.urlFoto = urlFoto;
	}
	
	/**
	 * @return la cantidad de energia de poder iniciales para los bichos
	 * de esta especie.
	 */
	public int getEnergiaInicial() 
	{
		return this.energiaInicial;
	}
	
	public void setEnergiaIncial(int energiaInicial) 
	{
		this.energiaInicial = energiaInicial;
	}

	/**
	 * @return el tipo de todos los bichos de esta especie
	 */
	@Enumerated(EnumType.ORDINAL) 
	public TipoBicho getTipo() 
	{
		return this.tipo;
	}
	
	public void setTipo(TipoBicho tipo) 
	{
		this.tipo = tipo;
	}
	
	/**
	 * @return la cantidad de bichos que se han creado para esta
	 * especie.
	 */
	public int getCantidadBichos() 
	{
		return this.cantidadBichos;
	}
	
	public void setCantidadBichos(int i) 
	{
		this.cantidadBichos = i;
	}

	public Especie getEvolucion() {
		return evolucion;
	}

	public void setEvolucion(Especie evolucion) {
		this.evolucion = evolucion;
	}

	public Especie getRaiz() {
		return raiz;
	}

	public void setRaiz(Especie raiz) {
		this.raiz = raiz;
	}

	public void setEnergiaInicial(int energiaInicial) {
		this.energiaInicial = energiaInicial;
	}

	public Evoluciones getTablaEvoluciones() {
		return this.tablaEvol;
	}

	public int getIdEspecie() {
		return idEspecie;
	}

	public void setIdEspecie(int idEspecie) {
		this.idEspecie = idEspecie;
	}

	public Evoluciones getTablaEvol() {
		return tablaEvol;
	}

	public void setTablaEvol(Evoluciones tablaEvol) {
		this.tablaEvol = tablaEvol;
	}

	public void sumarUnBicho() {
		this.cantidadBichos += 1;
		
	}
	
}
