package ar.edu.unq.epers.bichomon.backend.service.bichoService.imp;

public class BusquedaFallidaException extends RuntimeException {
	public BusquedaFallidaException(String m){
		super(m);
	}
}
