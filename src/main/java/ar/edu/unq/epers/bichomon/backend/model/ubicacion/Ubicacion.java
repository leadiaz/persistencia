package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.BichoNoEncontradoException;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Resultado;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)  
public abstract class Ubicacion {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int idUbicacion;
	protected String nombreUbicacion;
	@OneToMany(fetch = FetchType.EAGER, mappedBy="ubicacionActual", cascade=CascadeType.ALL)
	protected Set <Entrenador> entrenadoresEnElLugar;
	
	
	
	public int getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(int idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getNombreUbicacion() {
		return nombreUbicacion;
	}

	public void setNombreUbicacion(String nombreUbicacion) {
		this.nombreUbicacion = nombreUbicacion;
	}

	public void agregarEntrenador(Entrenador entrenador){
		this.entrenadoresEnElLugar.add(entrenador);
	}
	public Set<Entrenador> getEntrenadoresEnElLugar() {
		return entrenadoresEnElLugar;
	}
	
	public int cantidadDeEntrenadores(){
		return entrenadoresEnElLugar.size();
	}

	public void setEntrenadoresEnElLugar(Set<Entrenador> entrenadoresEnElLugar) {
		this.entrenadoresEnElLugar = entrenadoresEnElLugar;
	}

	public void abandonar(Bicho bichoHuerfano){		
		throw new UbicacionIncorrectaException("Wacho hdp hacete cargo del pokepibe");
	}

	public Resultado retar(Bicho elBicho){		
		throw new UbicacionIncorrectaException("Quien te conoce boludo? Anda a la cancha bobo");
	}

	public Bicho buscarEnLugar(Entrenador entrenador){		
		throw new BichoNoEncontradoException("No podes buscar bichos acá");
	}
}

