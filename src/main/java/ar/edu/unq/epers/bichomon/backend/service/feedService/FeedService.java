package ar.edu.unq.epers.bichomon.backend.service.feedService;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;

public interface FeedService {
	
	
	List<Evento> feedEntrenador(String entrenador);
	List<Evento> feedUbicacion(String entrenador);
}
