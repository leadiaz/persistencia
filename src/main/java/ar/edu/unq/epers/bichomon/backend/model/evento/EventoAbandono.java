package ar.edu.unq.epers.bichomon.backend.model.evento;

public class EventoAbandono extends Evento {
	
	String nombreEspecieDelBichoAbandonado;
	
	public EventoAbandono() {
	}
	
	public String getNombreEspecieDelBichoAbandonado() {
		return nombreEspecieDelBichoAbandonado;
	}

	public void setNombreEspecieDelBichoAbandonado(String nombreEspecieDelBichoAbandonado) {
		this.nombreEspecieDelBichoAbandonado = nombreEspecieDelBichoAbandonado;
	}

	public EventoAbandono(String nombreEnt, String nombreUbi, String nombreEsp) {
		super(nombreEnt, nombreUbi);
		this.nombreEspecieDelBichoAbandonado = nombreEsp;
	}
}
