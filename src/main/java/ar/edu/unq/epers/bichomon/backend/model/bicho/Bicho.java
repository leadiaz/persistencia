package ar.edu.unq.epers.bichomon.backend.model.bicho;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.IndexColumn;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.Evoluciones;
import ar.edu.unq.epers.bichomon.backend.model.especie.NoPuedeEvolucionarException;
/**
 * Un {@link Bicho} existente en el sistema, el mismo tiene un nombre
 * y pertenece a una {@link Especie} en particular.
 * 
 * @author Charly Backend
 */
@Entity
public class Bicho{
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBicho;
	private int energia;
	private int cantidadVictorias;
	@ManyToOne(optional = false)
	private Especie especie;	
	@ManyToOne
	private Entrenador owner;
	private LocalDateTime fechaCaptura;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@IndexColumn(name="LIST_INDEX")
	@JoinColumn(name="idExEntrenador")
	List<Entrenador> exEntrenadores;
	
	public Bicho() {
		this.exEntrenadores = new ArrayList<Entrenador>();
	}
	
	public Bicho(Especie especie){
		this.especie = especie;
		this.energia = especie.getEnergiaInicial();
		this.cantidadVictorias = 0;
		this.exEntrenadores = new ArrayList<Entrenador>();
		this.especie.sumarUnBicho();
	}

	/**
	 * @return la especie a la que este bicho pertenece.
	 */
	public Especie getEspecie(){
		return this.especie;
	}
	
	
	/**
	 * @return la cantidad de puntos de energia de este bicho en
	 * particular. Dicha cantidad crecerá (o decrecerá) conforme
	 * a este bicho participe en combates contra otros bichomones.
	 */
	public int getEnergia() 
	{
		return this.energia;
	}
	
	public int getCantidadVictorias(){
		return this.cantidadVictorias;
	}
	
	public Entrenador getOwner() {
		return owner;
	}
	public void setOwner(Entrenador owner) {
		this.owner = owner;
	}
	public void setId(int id) {
		this.idBicho = id;
	}
	public void setEnergia(int energia) 
	{
		this.energia = energia;
	}
	
	public void evolucionar(){
		Especie proxEvol = this.especie.getEvolucion();
		if(proxEvol != null){
			this.setEspecie(proxEvol);	
		}else{
			throw new NoPuedeEvolucionarException("Este bicho no tiene mas evoluciones");
		}
	}

	private void setEspecie(Especie proxEvol) {
		
		this.especie = proxEvol;
	}

	public void incrementarEnergia() {
		
		int random = 1 + (int)(Math.random() * ((5 - 1) + 1));
		this.energia = this.energia + random;
	}

	public int getId() {
		
		return idBicho;
	}

	public void incrementarVictoria() {
		this.cantidadVictorias =+ 1;
		
	}

	public void setNacimiento() {
		this.fechaCaptura = LocalDateTime.now();
		
	}
	
	public LocalDateTime getFechaCaptura(){
		return this.fechaCaptura;
	}

	public Evoluciones getTablaEvoluciones() {
		return this.especie.getTablaEvoluciones();
	}

	public void setFechaCapura(LocalDateTime fecha) {
		this.fechaCaptura = fecha;
	}

	public int getIdBicho() {
		return idBicho;
	}

	public void setIdBicho(int idBicho) {
		this.idBicho = idBicho;
	}

	public List<Entrenador> getExEntrenadores() {
		return exEntrenadores;
	}

	public void setExEntrenadores(List<Entrenador> exEntrenadores) {
		this.exEntrenadores = exEntrenadores;
	}

	public void setCantidadVictorias(int cantidadVictorias) {
		this.cantidadVictorias = cantidadVictorias;
	}

	public void setFechaCaptura(LocalDateTime fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	public void nuevoExEntrenador(Entrenador entrenador) {
		this.exEntrenadores.add(entrenador);
		this.owner = null;
	}

	public Boolean esExEntrenador(Entrenador entrenador) {
		return this.exEntrenadores.stream().anyMatch(e -> e.getId() == entrenador.getId());
	}

	public Especie getRaizSiTiene() {
		Especie res;
		if(this.especie.getRaiz() == null){
			res = this.especie;
		}else{
			res = this.especie.getRaiz();
		}
		return res;
	}

	public boolean puedeEvolucionar() {
		return(this.getTablaEvoluciones().puedeEvolucionar(this, this.getOwner().getNivel()));

	}
}
