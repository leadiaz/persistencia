package ar.edu.unq.epers.bichomon.backend.service.ubicacion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProveedorCostos {

	Map<String, Integer> costos;
	
	public ProveedorCostos(){
		this.costos = new HashMap<String, Integer>();
		costos.put("aereo", 5);
		costos.put("terrestre", 1);
		costos.put("maritimo", 2);
	}
	
	public int getCostoTotal(List<String>tipos){
		
		System.out.println(tipos.size());
		int total = 0;
		for(String t :tipos){
			total += this.costos.get(t);
		}
		
		return total;
	}

	public int getMenorCosto(List<String> tipos) {		
		List<Integer>costos = new ArrayList<>();
		for(String t : tipos){
			costos.add(this.costos.get(t));
		}
		Collections.sort(costos);
		return costos.get(0);
	}

	public List<String> getRutaMenorCosto(List<List<String>> relacionesEntreMedio) {
		List<String> rutaResul = null;
		for(int i =0; i< relacionesEntreMedio.size(); i++){
			if(rutaResul == null){
				rutaResul = relacionesEntreMedio.get(i);
			}
			if(this.getCostoTotal(rutaResul) < this.getCostoTotal(relacionesEntreMedio.get(i))){
				rutaResul = relacionesEntreMedio.get(i);
			}
		}
		
		return rutaResul;
	}
	
	
	
	
}
