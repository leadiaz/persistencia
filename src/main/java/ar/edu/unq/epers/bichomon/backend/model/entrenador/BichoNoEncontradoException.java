package ar.edu.unq.epers.bichomon.backend.model.entrenador;

public class BichoNoEncontradoException extends RuntimeException {
	public BichoNoEncontradoException(String string){
		super(string);
	}
}
