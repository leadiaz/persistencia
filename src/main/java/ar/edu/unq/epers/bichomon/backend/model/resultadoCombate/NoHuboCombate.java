package ar.edu.unq.epers.bichomon.backend.model.resultadoCombate;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public class NoHuboCombate implements Resultado{

	private Bicho ganador; 
	
	public NoHuboCombate(Bicho ganador){
		this.ganador = ganador;
	}

	@Override
	public Bicho getGanador() {
		return ganador;
	}

	@Override
	public List<Ronda> getDetalleDeLaPelea() {
		return null;
	}

	@Override
	public Bicho getPerdedor() {
		return null;
	}
}
