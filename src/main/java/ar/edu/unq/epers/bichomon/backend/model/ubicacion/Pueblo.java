package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;
import org.hibernate.annotations.IndexColumn;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.WrapperEspecie;

@Entity
@PrimaryKeyJoinColumn(name="ID")  
public class Pueblo extends Ubicacion{	
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@IndexColumn(name="LIST_INDEX")
	@JoinColumn(name="idEspecieEnPueblo")
	List<WrapperEspecie> especiesEnPueblo;//cambiar a wrapper de EspecieConProbabilidad
	@Transient
	Random random;
	
	public Pueblo(){
		this.random = new Random();
	}
	
	public Pueblo(String nombre){
		this.nombreUbicacion = nombre;
		this.entrenadoresEnElLugar = new HashSet<Entrenador>();
		this.especiesEnPueblo = new ArrayList<WrapperEspecie>();
		this.random = new Random();
	}
	
	public Pueblo(Integer id, String nombre){
		this.idUbicacion = id;
		this.nombreUbicacion = nombre;
		this.entrenadoresEnElLugar = new HashSet<Entrenador>();
		this.especiesEnPueblo = new ArrayList<WrapperEspecie>();
		this.random = new Random();
	}
	
	public Bicho buscarEnLugar(Entrenador entrenador){
		if(this.especiesEnPueblo.size() == 0){
			throw new NoHayBichosException("No hay bichos en el pueblo");
		}
		Especie encontrada = this.buscarEspecie();
		return new Bicho(encontrada);
	}
	
	public void agregarEspecie(Especie especie, Integer prob){ 
		//hacer un wrapper especie en pueblo y que arriba le pasen la probabilidad y que implemente el mensaje getProbabilidadDebeSerEncontrado()
		WrapperEspecie wrapperEsp = new WrapperEspecie(especie, prob);
		this.especiesEnPueblo.add(wrapperEsp);
		this.especiesEnPueblo.sort((e1, e2) -> e1.getProbabilidadDeSerEncontrado().compareTo(e2.getProbabilidadDeSerEncontrado()));
	}
	
	public Especie buscarEspecie(){
		Integer maxProbabilidad = this.especiesEnPueblo.get(especiesEnPueblo.size() - 1).getProbabilidadDeSerEncontrado();
		Integer probabilidad = random.nextInt(maxProbabilidad);
		WrapperEspecie res = especiesEnPueblo.stream()
											 .filter(e -> e.getProbabilidadDeSerEncontrado() >= probabilidad)
											 .findFirst()
											 .get();
		return res.getEspecie();
	}
	
	public void setRandom(Random random) {
		this.random = random;
	}

	public List<WrapperEspecie> getEspeciesEnPueblo(){
		return this.especiesEnPueblo;
	}
}
