package ar.edu.unq.epers.bichomon.backend.service.leaderboardService.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.leaderboardService.LearderBoardService;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;

public class LeaderboardServiceImpl implements LearderBoardService{
	
	DataHibernateDao dao;
	CacheCampeones cacheCamp;
	
	 public LeaderboardServiceImpl(DataHibernateDao dao, CacheCampeones cacheCamp){
		this.dao = dao;
		this.cacheCamp = cacheCamp;
	}
	@Override
	public List<Entrenador> campeones() {
		List<Entrenador> campeones = cacheCamp.getAll();
		if(campeones.isEmpty()){
			return Runner.runInSession(() -> {
				return this.dao.getCampeones(); 
			});
		}
		return campeones;
	}

	@Override
	public Especie especieLider() {
		return (Especie) Runner.runInSession(() -> {
			return this.dao.getEspecieLider();
		});
	}
	
//BONUS//
	@Override
	public List<Entrenador> lideres() {
		return Runner.runInSession(() -> {
			return this.dao.getLideres(); 
		});
	}

}