package ar.edu.unq.epers.bichomon.backend.service.mapaService;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public interface MapaService {


	public abstract void mover(String entrenador, String ubicacion);
	/*se cambiará al entrenador desde su ubicación actual a la especificada por parametro.
	 * 
	 */
	
	public abstract void moverMasCorto(String entrenador, String ubicacion);

	public abstract int cantidadEntrenadores(String ubicacion);
	/*se deberá devolver la cantidad de entrenadores que se encuentren actualmente en dicha localización.
	 * 
	 */

	//BONUS:
	public abstract Bicho campeon(String dojo);
	/*retorna el actual campeon del Dojo especificado.
	 * 
	 */
	//BONUS:
	public abstract Bicho campeonHistorico(String dojo);
	/*retorna el bicho que haya sido campeon por mas tiempo en el Dojo.
	 * 
	 */
	
}
