package ar.edu.unq.epers.bichomon.backend.service.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import redis.clients.jedis.Jedis;

public class CacheCampeones {

	private Jedis jedis;

    public CacheCampeones() {
        this.jedis = CacheProvider.getInstance().getJedis();
    }

    public void clear() {
        jedis.flushAll();
    }

    public void put(Dojo key, Entrenador value) {
        jedis.set(generateKey(key), JsonSerializer.toJson(value));
    }

    public Entrenador get(Dojo key) {
    	String k = generateKey(key);
        return this.getConString(k);
    }
    
    private Entrenador getConString(String k){
    	String value = this.jedis.get(k);
    	return (Entrenador) JsonSerializer.fromJsonObject(value, Entrenador.class);
    }

    
    public List<Entrenador> getAll(){
    	Set<String> keys = this.jedis.keys("*");
    	List<Entrenador> res = new ArrayList<Entrenador>();
    	for(String k : keys ){
    		res.add(this.getConString(k));
    	}
    	return res;
    }

    public int size() {
        return this.jedis.keys("*").size();
    }

    protected String generateKey(Dojo ubi){
        return ubi.getNombreUbicacion();
    }
}
