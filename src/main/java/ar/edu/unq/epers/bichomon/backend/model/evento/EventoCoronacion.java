package ar.edu.unq.epers.bichomon.backend.model.evento;

public class EventoCoronacion extends Evento {
	
	String nombreEntrenadorDestronado;
	
	public EventoCoronacion() {
	}
	
	public EventoCoronacion( String nombreEnt, String nombreUbi, String nombreEntDestronado) {
		super(nombreEnt, nombreUbi);
		this.nombreEntrenadorDestronado = nombreEntDestronado;
	}

	public String getNombreEntrenadorDestronado() {
		return nombreEntrenadorDestronado;
	}

	public void setNombreEntrenadorDestronado(String nombreEntrenadorDestronado) {
		this.nombreEntrenadorDestronado = nombreEntrenadorDestronado;
	}
}
