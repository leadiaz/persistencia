package ar.edu.unq.epers.bichomon.backend.service.feedService.impl;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.feedService.FeedService;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class FeedServiceImpl implements FeedService {
	
	EventoDAO dao;
	DataServiceImp service;
	UbicacionDao daoU;
	
	
	 public FeedServiceImpl(EventoDAO daoE, DataServiceImp service, UbicacionDao daoU){
		this.dao = daoE;
		this.service = service;
		this.daoU = daoU;
	}
	@Override
	public List<Evento> feedEntrenador(String entrenador) {
		return dao.getByNombreEntrenador(entrenador); 
	}

	@Override
	public List<Evento> feedUbicacion(String entrenador) {
		Entrenador elEntrenador = this.service.recuperarPorColumna(Entrenador.class, entrenador, "nombre");
		String ubicacion = elEntrenador.getUbicacionActual().getNombreUbicacion();
		List<String> ubicaciones = this.daoU.todosLosConectados(ubicacion);
		return dao.getByNombresUbicaciones(ubicaciones) ;
	}

}
