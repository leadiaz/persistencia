package ar.edu.unq.epers.bichomon.backend.model.especie;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

@Entity
public class Evoluciones {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int energia;
	private int victorias;
	private LocalDateTime edad;
	private int nivel;
	
	public Evoluciones(int nrg, int wins, LocalDateTime age, int lvl){
		this.energia = nrg;
		this.victorias = wins;
		this.edad = age;
		this.nivel = lvl;
	}
	
	public Evoluciones(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}

	public void setVictorias(int victorias) {
		this.victorias = victorias;
	}

	public void setEdad(LocalDateTime edad) {
		this.edad = edad;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getEnergia() {
	
		return this.energia;
	}
	
	public int getVictorias(){
		return this.victorias;
	}
	
	public LocalDateTime getEdad(){
		return this.edad;
	}
	
	public int getNivel(){
		return this.nivel;
	}
	
	public Boolean puedeEvolucionar(Bicho bicho, Integer nivel){
		return cumpleVictorias(bicho.getCantidadVictorias()) && cumpleEnergia(bicho.getEnergia()) &&
			   cumpleEdad(bicho.getFechaCaptura()) && cumpleNivel(nivel);
	}
	
	private boolean cumpleEdad(LocalDateTime fechaCaptura) {
		if(this.edad != null){
			return fechaCaptura.isBefore(this.edad); 
		}
		return true;
	}

	private boolean cumpleNivel(Integer nivel) {
		return nivel >= this.nivel;
	}

	private boolean cumpleEnergia(int energia) {
		return energia >= this.energia;
	}

	private boolean  cumpleVictorias(Integer victorias){
		return victorias >= this.victorias;
	}
}
