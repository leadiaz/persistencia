package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import org.hibernate.annotations.IndexColumn;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.HistorialCampeon;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.ModoCombate;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.NoHuboCombate;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Resultado;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.ResultadoCombate;

@Entity
public class Dojo extends Ubicacion{
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="idBicho")
	Bicho bichoCampeon; 
	@OneToMany(mappedBy="dojo",cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@IndexColumn(name="LIST_INDEX")
	List<HistorialCampeon> historialCampeones;  
	@Transient
	ModoCombate combate;
	
	public Dojo(String string) {
		this.combate = new ModoCombate();
		this.setNombreUbicacion(string);
		this.historialCampeones = new ArrayList<HistorialCampeon>();
	}
	
	public Dojo(int id,String string) {
		this.idUbicacion = id;
		this.combate = new ModoCombate();
		this.setNombreUbicacion(string);
		this.historialCampeones = new ArrayList<HistorialCampeon>();
	}

	public Dojo() {
		this.combate = new ModoCombate();
	}
	
	public Resultado retar(Bicho bichoRetador){
		
		if(!this.hayCampeon()){
			this.setNuevoCampeon(bichoRetador);
			this.historialCampeones.add(new HistorialCampeon(bichoRetador, this));
			return new NoHuboCombate(bichoRetador);
		}
		return this.combatir(bichoRetador);
	}
	
	public Boolean hayCampeon(){
		return this.bichoCampeon != null;
	}

	public void setNuevoCampeon(Bicho bichoRetador) {	
		bichoCampeon = bichoRetador;
	}

	private ResultadoCombate combatir(Bicho bichoRetador) {
		ResultadoCombate resultado = this.combate.combatir(bichoRetador, bichoCampeon);
		bichoRetador.incrementarEnergia();
		bichoCampeon.incrementarEnergia();
		Bicho nuevoGanador = resultado.getGanador();
		nuevoGanador.incrementarVictoria();
		if(nuevoGanador != bichoCampeon){
			this.setNuevoCampeon(nuevoGanador);	
			Integer index = this.historialCampeones.size() - 1; 
			this.historialCampeones.get(index).setFechaFin(); 
			this.historialCampeones.add(new HistorialCampeon(bichoRetador, this));		
		}
		return resultado;
	}
	
	public Bicho getBichoCampeon(){
		return bichoCampeon;
	}

	public List<HistorialCampeon> getHistorialCampeones() {
		return historialCampeones;
	}

	public void setHistorialCampeones(List<HistorialCampeon> historialCampeones) {
		this.historialCampeones = historialCampeones;
	}

	public ModoCombate getCombate() {
		return combate;
	}

	public void setCombate(ModoCombate combate) {
		this.combate = combate;
	}

	public void setBichoCampeon(Bicho bichoCampeon) {
		this.bichoCampeon = bichoCampeon;
	}	
	
	public Bicho buscarEnLugar(Entrenador entrenador){
		if(this.bichoCampeon != null){
			Bicho nuevoBicho; 
			nuevoBicho = new Bicho(this.bichoCampeon.getRaizSiTiene());
			return nuevoBicho;
		}
		else{
			throw new NoHayBichosException("No hay ningun campeon en este dojo");
		}
				
	}
}