package ar.edu.unq.epers.bichomon.backend.model.resultadoCombate;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public class Ronda {
	Bicho atacante;
	Bicho atacado;
	double danhoAtacante;
	
	public Ronda(Bicho atacante, double danhoAtacante, Bicho atacado){
		this.atacante = atacante;
		this.danhoAtacante = danhoAtacante;
		this.atacado = atacado;
	}

	public Bicho getAtacante() {
		return atacante;
	}

	public Bicho getAtacado() {
		return atacado;
	}

	public double getDanhoAtacante() {
		return danhoAtacante;
	}
}
