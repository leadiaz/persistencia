package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.IndexColumn;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.BichoNoEncontradoException;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;

@Entity
@PrimaryKeyJoinColumn(name="ID")  
public class Guarderia extends Ubicacion{
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@IndexColumn(name="LIST_INDEX")
	@JoinColumn(name="idBichoAbandonado")
	List<Bicho> bichosAbandonados;
	
	public Guarderia(){
	}
	
	public Guarderia(String nombre){
		this.nombreUbicacion = nombre;
		this.entrenadoresEnElLugar = new HashSet<Entrenador>(); 
		this.bichosAbandonados = new ArrayList<Bicho>();
	}
	
	public Guarderia(Integer id, String nombre){
		this.idUbicacion = id;
		this.nombreUbicacion = nombre;
		this.entrenadoresEnElLugar = new HashSet<Entrenador>(); 
		this.bichosAbandonados = new ArrayList<Bicho>();
	}
	

	public void abandonar(Bicho bichoHuerfano){
		bichosAbandonados.add(bichoHuerfano);
	}

	public List<Bicho> getBichosAbandonados() {
		return bichosAbandonados;
	}

	public void setBichosAbandonados(List<Bicho> bichosAbandonados) {
		this.bichosAbandonados = bichosAbandonados;
	}
	
	public Bicho buscarEnLugar(Entrenador entrenador){	
		List<Bicho> bichosPosibles = this.bichosAbandonados.stream()
				.filter(b -> !b.esExEntrenador(entrenador)).collect(Collectors.toList());
		if(bichosPosibles.isEmpty()){
			throw new NoHayBichosException("No hay bicho para adoptar");
		}
		Bicho bicho = bichosPosibles.get(0);
		this.bichosAbandonados.remove(bicho);
		return bicho;			
	}
}
