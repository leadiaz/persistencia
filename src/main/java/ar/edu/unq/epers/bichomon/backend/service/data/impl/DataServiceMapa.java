package ar.edu.unq.epers.bichomon.backend.service.data.impl;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.service.data.DataService;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;

public class DataServiceMapa implements DataService{
	
	private MapaServiceImp mapaService;
	
	public DataServiceMapa(MapaServiceImp mapaService){
		this.mapaService = mapaService;
		
	}
	@Override
	public void eliminarDatos() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void crearSetDatosIniciales() {	
		
		
		Guarderia stblah = new Guarderia(1, "St.Blah");
		Pueblo plantaland = new Pueblo(2, "Plantaland");
		Pueblo agualand = new Pueblo(3, "Agualand");
		Pueblo lagartoland = new Pueblo(4, "Lagartoland");
		Pueblo bicholand = new Pueblo(5, "Bicholand");
		Dojo tibetDojo = new Dojo(6, "Tibet Dojo");
		
		Entrenador e = new Entrenador("ash");
		e.setUbicacionActual(plantaland);
		mapaService.crearUbicacion(stblah);
		mapaService.crearUbicacion(plantaland);
		mapaService.crearUbicacion(agualand);
		mapaService.crearUbicacion(lagartoland);
		mapaService.crearUbicacion(bicholand);
		mapaService.crearUbicacion(tibetDojo);
		
		mapaService.conectar("St.Blah", "Agualand", "terrestre");
		mapaService.conectar("Agualand", "St.Blah", "terrestre");
		mapaService.conectar("St.Blah", "Plantaland", "aereo");
		mapaService.conectar("Agualand", "Plantaland", "maritimo");
		mapaService.conectar("Plantaland", "Agualand", "maritimo");
		mapaService.conectar("Agualand", "Lagartoland", "maritimo");
		mapaService.conectar("Lagartoland", "Agualand", "maritimo");
		mapaService.conectar("Agualand", "Bicholand", "maritimo");
		mapaService.conectar("Bicholand", "Lagartoland", "terrestre");
		mapaService.conectar("Lagartoland", "Bicholand", "terrestre");
		mapaService.conectar("Bicholand", "Tibet Dojo", "aereo");
		mapaService.conectar("Tibet Dojo", "Bicholand", "aereo");
		mapaService.conectar("Tibet Dojo", "Plantaland", "terrestre");		
	}

}


















