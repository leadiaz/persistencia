package ar.edu.unq.epers.bichomon.backend.model.entrenador;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.especie.Evoluciones;
import ar.edu.unq.epers.bichomon.backend.model.especie.NoPuedeEvolucionarException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.CaminoMuyCostosoException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionMuyLejanaException;

@Entity
public class Entrenador {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	public void setId(int id) {
		this.id = id;
	}

	private String nombre;
	private int experiencia;
	private int nivel;
	@OneToMany(fetch = FetchType.EAGER, mappedBy="owner", cascade=CascadeType.ALL)
	private List<Bicho> bichosCapturados; 	
	@ManyToOne
	private Ubicacion ubicacionActual;
	private int limiteDeBichos;
	private int monedas;
	
	public Entrenador(){
	}
	
	public Entrenador(String nombre){
		this.nombre = nombre;	
		this.experiencia = 0;
		this.nivel = 1;
		this.bichosCapturados = new ArrayList<Bicho>();
		this.limiteDeBichos = 100;
		this.monedas = 100;
	}		

	public int getNivel() {
		return nivel;
	}
	
	public int getId() {
		return this.id;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}

	public List<Bicho> getBichosCapturados() {
		return bichosCapturados;
	}

	public void setBichosCapturados(List<Bicho> bichosCapturados) {
		this.bichosCapturados = bichosCapturados;
	}

	public int getLimiteDeBichos() {
		return limiteDeBichos;
	}

	public void setLimiteDeBichos(int limiteDeBichos) {
		this.limiteDeBichos = limiteDeBichos;
	}
	

	public void setUbicacionActual(Ubicacion ubicacionActual) {
		this.ubicacionActual = ubicacionActual;
		this.ubicacionActual.agregarEntrenador(this);
	}

	public Ubicacion getUbicacionActual() {	
		return this.ubicacionActual;
	}

	public boolean puedeCapturarBicho()
	{
		return (limiteDeBichos < bichosCapturados.size());
	}
	 

	public Bicho getBicho(int bicho) {
		
		for(int i = 0; i < this.bichosCapturados.size(); i++){
			
			if(this.bichosCapturados.get(i).getId() == bicho){
				return this.bichosCapturados.get(i);
			}
		}
		throw new BichoNoEncontradoException("Este entrenador no tiene ese pokemon");
	}
	
	public void agregarBicho(Bicho bicho){
		this.bichosCapturados.add(bicho);
		bicho.setNacimiento();
		bicho.setOwner(this);
	}
	
	public void abandonar(Bicho elBicho) {		
		if(this.bichosCapturados.size() > 1){
			this.bichosCapturados.remove(elBicho);
			elBicho.nuevoExEntrenador(this);
			this.ubicacionActual.abandonar(elBicho);			
		}
		else{
			throw new NoHayBichosException("No podes abandonar tu ultimo bicho");
		}		
	}

//	public boolean puedeEvolucionar(Bicho elBicho, Evoluciones tablaEvol) {		
//		if(tablaEvol.puedeEvolucionar(elBicho, nivel)){
////			return true;
////		}else{
////			 throw new NoPuedeEvolucionarException("Este bicho no puede evolucionar");
////		} 
//	}

	public void evolucionar(Bicho elBicho) {		
		elBicho.evolucionar();
	}

	public void mover(int costoViaje, Ubicacion laUbicacion) {
		if(costoViaje <= this.monedas){
			this.setUbicacionActual(laUbicacion);
			this.monedas = this.monedas - costoViaje;
		}
		else{
			throw new CaminoMuyCostosoException("No tenes un sope vieja");
		}	
	}

	public int getMonedas() {
		return monedas;
	}

	public void setMonedas(int monedas) {
		this.monedas = monedas;
	}
	
	public void actualizarNivel(Integer exp, Nivel nivel) {
		this.setExperiencia(this.getExperiencia() + exp);
		this.setNivel(nivel.getNivel(this.getExperiencia()));
		
	}	
}
