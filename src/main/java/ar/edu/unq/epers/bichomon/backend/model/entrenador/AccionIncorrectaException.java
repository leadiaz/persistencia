package ar.edu.unq.epers.bichomon.backend.model.entrenador;

public class AccionIncorrectaException extends RuntimeException {
	public AccionIncorrectaException(String string){
		super(string);
	}
}
