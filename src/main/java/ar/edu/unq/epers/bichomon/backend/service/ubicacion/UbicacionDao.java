package ar.edu.unq.epers.bichomon.backend.service.ubicacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.Values;

import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.*;


public class UbicacionDao {
	
	private Driver driver;
	public UbicacionDao() {
		this.driver = GraphDatabase.driver( "bolt://localhost", AuthTokens.basic( "neo4j", "1234" ) );
	}
	
	public void crearUbicacion(Ubicacion ubicacion) {
		Session session = this.driver.session();
		
		try {
			String query = "MERGE (ub:"+ubicacion.getClass().getSimpleName()+ ":Ubicacion {id: {id}}) " +
					"SET ub.nombre = {nombre} ";
			session.run(query, Values.parameters("nombre", ubicacion.getNombreUbicacion(),
					"id", ubicacion.getIdUbicacion()));

		} finally {
			session.close();
		}
	}
	
	//Retorna el nombre de la ubicacion y la id
	public Map<String, Integer> getUbicacion(Integer idUbicacion) {
		Session session = this.driver.session();

		try {
			String query = "MATCH (u1:Ubicacion {id: {id1}}) RETURN u1.nombre, u1.id";
			StatementResult result =session.run(query, Values.parameters("id1", idUbicacion)); 
			Map<String, Integer> nombres = new HashMap<String, Integer>();
			result.list(a -> {
				String nombre = a.get(0).asString();
				Integer id = a.get(1).asInt();
				nombres.put(nombre, id);	
				return null;
			});
			return nombres;
			
		} finally {
			session.close();
		}
	}
	
	public void conectar(String ubicacion1, String ubicacion2, String tipoConeccion) {
		Session session = this.driver.session();

		try {
			String query = "MATCH (u1:Ubicacion {nombre: {nombre1}}) " +
					"MATCH (u2:Ubicacion {nombre: {nombre2}}) " +
					"MERGE (u1)-[:"+tipoConeccion+"]->(u2) ";
			session.run(query, Values.parameters("nombre1", ubicacion1,
					"nombre2", ubicacion2));

		} finally {
			session.close();
		}
	}
	
	//Retorna el nombre y el tipo de ubicacion
	public Map<String, String> conectados(String ubicacion1, String tipoConeccion) {
		Session session = this.driver.session();

		try {
			String query = "MATCH (u1:Ubicacion {nombre: {nombre1}})-[a:"+tipoConeccion+"]->(r) RETURN r.nombre, head(labels(r))";
			StatementResult result =session.run(query, Values.parameters("nombre1", ubicacion1)); 
			Map<String, String> nombres = new HashMap<String, String>();  
			result.list(a -> {
				String nombre = a.get(0).asString();
				String tipo = "ar.edu.unq.epers.bichomon.backend.model.ubicacion."+a.get(1).asString();
				nombres.put(nombre, tipo);	
				return null;
			});
			return nombres;
			
		} finally {
			session.close();
		}
	}

	public List<String> todosLosConectados(String ubicacion1) {
		Session session = this.driver.session();

		try {
			String query = "MATCH (u1:Ubicacion {nombre: {nombre1}})-[]->(r) RETURN r.nombre";
			StatementResult result =session.run(query, Values.parameters("nombre1", ubicacion1)); 
			List<String> nombres = new ArrayList<String>();  
			result.list(a -> {
				String nombre = a.get(0).asString();
				nombres.add(nombre);	
				return null;
			});
			nombres.add(ubicacion1);
			return nombres;
			
		} finally {
			session.close();
		}
	}
	
	public List<List<String>> relacionesEntreMedio(Ubicacion ubicacionActual, Ubicacion laUbicacion) {
		Session session = this.driver.session();
		try{
			String query = "MATCH path=allShortestPaths((uA:Ubicacion{id:{idU1}})-[*]->(uD:Ubicacion{id: {idU2}})) " +
							"RETURN extract(t in relationships(path)| type(t)) as extracted";
			
			
			
			StatementResult result = session.run(query, Values.parameters("idU1", ubicacionActual.getIdUbicacion(), "idU2", laUbicacion.getIdUbicacion()));
			List<Record> r = result.list();
			List<List<String>> tipos = new ArrayList<List<String>>();

			for(int index=0; index < r.size(); index++){
				List<String> v = r.get(index).get("extracted").asList(a ->a.asString());
				tipos.add(v);
			}	 
			 
			//result.list(record -> tipos.add(record.get("extracted").asList(value -> value.asString())));
			
			 return tipos;
		}
		finally {
			session.close();
		}
		
	}
	
	public List<String> tiposPathMasCorto(Ubicacion ubicacionActual, Ubicacion laUbicacion){

		Session session = this.driver.session();
		try{
			
			String query = "MATCH path=shortestPath((uA:Ubicacion{id: {idU1}})-[*]->(uD:Ubicacion{id: {idU2}})) " +
							"RETURN extract(t in relationships(path)| type(t)) as extracted";
			
			StatementResult result = session.run(query, Values.parameters("idU1", ubicacionActual.getIdUbicacion(), "idU2", laUbicacion.getIdUbicacion()));
		
			return result.single().get("extracted").asList(a -> a.asString());
		}
		finally {
			session.close();
		}
	}
	
	
	public void eleminarDatos() {
		Session session = this.driver.session();
		
		try {
			String query = "MATCH (n) DETACH DELETE n";
			session.run(query);

		} finally {
			session.close();
		}
	}
}
