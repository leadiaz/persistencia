package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

public class CaminoMuyCostosoException extends RuntimeException {

	public CaminoMuyCostosoException(String string){
		super(string);
	}
}
