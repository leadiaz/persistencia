package ar.edu.unq.epers.bichomon.backend.model.entrenador;

public class NoHayBichosException extends RuntimeException {
	public NoHayBichosException(String string){
		super(string);
	}
}
