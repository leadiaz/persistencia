package ar.edu.unq.epers.bichomon.backend.model.ubicacion;

public class UbicacionMuyLejanaException extends RuntimeException {

	public UbicacionMuyLejanaException(String string){
		super(string);
	}
}
	
