package ar.edu.unq.epers.bichomon.backend.service.redis;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import redis.clients.jedis.Jedis;

public class CacheCantEntrenadores {
	
	private Jedis jedis;

    public CacheCantEntrenadores() {
        this.jedis = CacheProvider.getInstance().getJedis();
    }

    public void clear() {
        jedis.flushAll();
    }

    public void put(Ubicacion key, List<Entrenador> value) {
        jedis.set(generateKey(key), JsonSerializer.toJson(value));
    }

    public List<Entrenador> get(String key) {
        String value = this.jedis.get(key);
        return JsonSerializer.fromJsonList(value, Entrenador.class);
    }

    public int size() {
        return this.jedis.keys("*").size();
    }

    protected String generateKey(Ubicacion ubi){
        return ubi.getNombreUbicacion();
    }

}
