package ar.edu.unq.epers.bichomon.backend.service.data.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;
import ar.edu.unq.epers.bichomon.backend.service.runner.SessionFactoryProvider;

public class DataHibernateDao{ 
	
	public DataHibernateDao (){
	}
	
	public void guardar(Object object) {
		Session session = Runner.getCurrentSession();
		session.save(object);
	}
	
	public <T> T recuperar(Class<T> tipo, Serializable key) {
		Session session = Runner.getCurrentSession();
		return session.get(tipo, key);
	}

	public void update(Object object) {
		Session session = Runner.getCurrentSession();
		session.update(object);
	}

	public void eliminarDatos() {
		SessionFactoryProvider.destroy();
	}

	public <T> T recuperarPorColumna(Class<T> tipo, Serializable valor, String columna) {
		Session session = Runner.getCurrentSession();
		Criteria criteria = session.createCriteria(tipo);
		return (T) criteria.add(Restrictions.eq(columna, valor)).uniqueResult();	
	}
	
	public <T> void executeUpdate(String hql, Class<T> tipo) 	{					
		Session session = Runner.getCurrentSession();		
		Query<T> query = session.createQuery(hql, tipo);
		query.executeUpdate();
	}
	
	public <T> List<T> executeQuery(String hql){					
		Session session = Runner.getCurrentSession();		
		Query<T> query = session.createQuery(hql);
		return query.list(); 
	}

	public List<Especie> getAllEspecies() {
		return this.executeQuery("from Especie");
	}
	
	public List<Especie> getImpopulares() {
		String hql = "select esp "
				+ "from Guarderia as guarderia " 
				+ "join guarderia.bichosAbandonados as bichos "
				+ "join bichos.especie as esp "
				+ "group by esp "
				+ "order by count(esp.idEspecie) desc ";
		return this.executeQuery(hql);
	}
	
	public List<Especie> getPopulares() {
		String hql = "select esp "
				+ "from Entrenador as entrenador " 
				+ "join entrenador.bichosCapturados as bichos "
				+ "join bichos.especie as esp "
				+ "group by esp "
				+ "order by count(esp.idEspecie) desc ";
		return this.executeQuery(hql);
	}
	public List<Entrenador> getCampeones() {
		String hql = "select i.bichoCampeon.owner "
				+"from Dojo  as i "
				+"join i.historialCampeones as msg "
				+ "order by fechaInicio ";	
		return this.executeQuery(hql);
	}
	
	public Especie getEspecieLider() {
		String hql = " select esp "
				+ "from  Dojo  as i "
				+"join i.historialCampeones as h "
				+ "join h.bichoCampeon as bic "
				+ "join  bic.especie as esp "
				+ "group by esp "
				+ "order by count(esp.idEspecie) desc ";
		return (Especie) this.executeQuery(hql).get(0);
	}
	public List<Entrenador> getLideres() {
		String hql = "select ent "
				+"from Entrenador as ent "
				+ " join ent.bichosCapturados as b "
				+ " group by ent "	
				+"order by sum(b.energia) desc ";
		return this.executeQuery(hql);
	}
}
