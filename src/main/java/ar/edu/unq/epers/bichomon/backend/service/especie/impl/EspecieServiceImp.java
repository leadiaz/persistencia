package ar.edu.unq.epers.bichomon.backend.service.especie.impl;


import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.especie.EspecieService;
import ar.edu.unq.epers.bichomon.backend.service.runner.Runner;

public class EspecieServiceImp implements EspecieService{	
	private DataHibernateDao dao;
	
	public EspecieServiceImp(DataHibernateDao dao){		
		this.dao = dao;
	}
	
	@Override
	public void crearEspecie(Especie especie){
		Runner.runInSession(() -> {
			this.dao.guardar(especie);
			return null;
		});		
	}

	@Override
	public Especie getEspecie(String nombreEspecie){
		return Runner.runInSession(() -> {
			return this.dao.recuperarPorColumna(Especie.class, nombreEspecie, "nombre");
		});	
		
	}

	public void actualizarEspecie(Especie especie){
		Runner.runInSession(() -> {
			this.dao.update(especie);
			return null;
		});					
	}

	@Override
	public List<Especie> getAllEspecies(){	
		return Runner.runInSession(() -> {
			return this.dao.getAllEspecies();//executeQuery("from Especie");				
		});	
	}

	@Override
	public Bicho crearBicho(String nombreEspecie) 
	{
		return Runner.runInSession(() -> {
			Especie especie = this.dao.recuperarPorColumna(Especie.class, nombreEspecie, "nombre");
			Bicho nuevoBicho = new Bicho(especie);
//			especie.setCantidadBichos(especie.getCantidadBichos() + 1);
			this.dao.update(especie);
			this.dao.guardar(nuevoBicho);
			return nuevoBicho;			
		});	
	}	
	
	public void sumarBicho(Bicho bicho) 
	{
		Runner.runInSession(() -> {
			Especie especie = bicho.getEspecie();
			especie.setCantidadBichos(especie.getCantidadBichos() + 1);
			this.dao.update(especie);
			this.dao.guardar(bicho);
			return null;
		});	
	}

	@Override
	public List<Especie> populares() {
		return Runner.runInSession(() -> {
			return this.dao.getPopulares();
		});
	}

	@Override
	public List<Especie> impopulares() {
		return Runner.runInSession(() -> {
			return this.dao.getImpopulares();
		});
	}
	
}
