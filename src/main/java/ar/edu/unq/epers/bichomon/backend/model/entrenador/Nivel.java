package ar.edu.unq.epers.bichomon.backend.model.entrenador;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Nivel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer nivel_id;	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name="limites", joinColumns=@JoinColumn(name="nivel_id"))
	@Column(name="limite")
	private List<Integer> limites;
	
	public Nivel(){
	}
	
	public Nivel(List<Integer> limites){
		this.limites = limites;
	}

	public List<Integer> getLimites() {
		return limites;
	}

	public void setLimites(List<Integer> limites) {
		this.limites = limites;
	}

	public Integer getNivel(Integer exp){
		Integer nivel = 1;
		Integer size = limites.size();
		for(Integer i = 0; i < size; i++){
			if((limites.get(i) > exp) && (i != size - 1)){
				return i + 1;
			}	
			nivel++;
		}
		return nivel;
	}	
}	
