package ar.edu.unq.epers.bichomon.backend.model.evento;

public class EventoCaptura extends Evento{
	
	String nombreEspecieDelBichoCapturado;
	
	public EventoCaptura() {
	}
	
	public EventoCaptura(String nombreEnt, String nombreUbi, String nombreEsp) {
		super(nombreEnt, nombreUbi);
		this.nombreEspecieDelBichoCapturado = nombreEsp;
	}

	public String getNombreEspecieDelBichoCapturado() {
		return nombreEspecieDelBichoCapturado;
	}

	public void setNombreEspecieDelBichoCapturado(String nombreEspecieDelBichoCapturado) {
		this.nombreEspecieDelBichoCapturado = nombreEspecieDelBichoCapturado;
	}	
}
