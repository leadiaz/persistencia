package ar.edu.unq.epers.bichomon.backend.model.resultadoCombate;

import java.util.List;
import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public class ResultadoCombate implements Resultado{

	List<Ronda> detalleDePelea;
	Bicho ganador;
	Bicho perdedor;
	
	public ResultadoCombate(Bicho ganador, Bicho perdedor, List<Ronda> detalleDePelea ){
		this.ganador = ganador;
		this.perdedor = perdedor;
		this.detalleDePelea = detalleDePelea;
	}

	public void setResultado(List<Ronda> detalle) {
		this.detalleDePelea = detalle;	
	}

	public void setGanadorPerdedor(Bicho pokePegador, Bicho pokeRecibidor) {
		
		this.ganador = pokePegador; 
		this.perdedor = pokeRecibidor;
	}

	@Override
	public Bicho getGanador() {
		return ganador;
	}
	
	public Bicho getPerdedor() {
		return perdedor;
	}
	
	public List<Ronda> getDetalleDeLaPelea() {
		return detalleDePelea;
	}
}
