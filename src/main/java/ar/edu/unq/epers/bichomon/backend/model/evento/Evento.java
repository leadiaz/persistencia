package ar.edu.unq.epers.bichomon.backend.model.evento;

import java.util.Date;

import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, 
			  include = JsonTypeInfo.As.PROPERTY,
			  property = "type"
)
@JsonSubTypes({
        @Type(value = EventoArribo.class, name = "arribo"),
        @Type(value = EventoAbandono.class, name = "abandono"),
        @Type(value = EventoCaptura.class, name = "captura"),
        @Type(value = EventoCoronacion.class, name = "coronacion"),
        @Type(value = EventoCoronacionDefault.class, name = "coronacionDefault")
})

public abstract class  Evento {
	@MongoId
	@MongoObjectId
	protected String id;
	protected String nombreEntrenador;
	protected String nombreUbicacion;
	private Date fechaInicioEvento;  
	
	//Se necesita un constructor vacio para que jackson pueda
		//convertir de JSON a este objeto.
	protected Evento() {
	}
		
	public Evento(String nombreEnt, String nombreUbi) {
			this.nombreEntrenador = nombreEnt;
			this.nombreUbicacion = nombreUbi;
			this.fechaInicioEvento = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombreEntrenador() {
		return nombreEntrenador;
	}

	public void setNombreEntrenador(String nombreEntrenador) {
		this.nombreEntrenador = nombreEntrenador;
	}

	public String getNombreUbicacion() {
		return nombreUbicacion;
	}

	public void setNombreUbicacion(String nombreUbicacion) {
		this.nombreUbicacion = nombreUbicacion;
	}

	public Date getFechaInicioEvento() {
		return fechaInicioEvento;
	}

	public void setFechaInicioEvento(Date fechaInicioEvento) {
		this.fechaInicioEvento = fechaInicioEvento;
	}
}	
	