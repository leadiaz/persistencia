package ar.edu.unq.epers.bichomon.backend.model.especie;

public class NoPuedeEvolucionarException extends RuntimeException {
	public NoPuedeEvolucionarException(String mensaje){
		super(mensaje);
	}

}
