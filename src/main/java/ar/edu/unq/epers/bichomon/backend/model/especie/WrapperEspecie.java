package ar.edu.unq.epers.bichomon.backend.model.especie;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
@Entity
public class WrapperEspecie {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEspecieEnPueblo;
	@OneToOne
	private Especie especie;
	private Integer probabilidadDeSerEncontrado;
	
	public WrapperEspecie(){
	}
	
	public WrapperEspecie(Especie unaEspecie, Integer probabilidad){
		this.especie = unaEspecie;
		this.probabilidadDeSerEncontrado = probabilidad;
	}

	public Integer getProbabilidadDeSerEncontrado() {
		return this.probabilidadDeSerEncontrado;
	}

	public Especie getEspecie() {
		return this.especie;
	}
}
