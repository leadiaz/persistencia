package ar.edu.unq.epers.bichomon.backend.service.leaderboardService;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;

public interface LearderBoardService {
	
	public abstract List<Entrenador> campeones();
	/*retorna aquellos entrenadores que posean un bicho 
	 * que actualmente sea campeon de un Dojo, retornando primero aquellos 
	 * que ocupen el puesto de campeon desde hace mas tiempo.
	 */

	public abstract Especie especieLider();
	/*retorna la especie que tenga mas bichos que haya sido campeones de cualquier dojo.
	 *  Cada bicho deberá ser contando una sola vez 
	 *  (independientemente de si haya sido coronado campeon mas de una vez 
	 *  o en mas de un Dojo)
	 */

	//BONUS:
	public abstract List<Entrenador> lideres();
	/*retorna los diez primeros entrenadores para los cuales el 
	 * valor de poder combinado de todos sus bichos sea superior.
	 */
}

