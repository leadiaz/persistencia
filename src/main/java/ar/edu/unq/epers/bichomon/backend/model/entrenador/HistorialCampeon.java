package ar.edu.unq.epers.bichomon.backend.model.entrenador;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;

@Entity
public class HistorialCampeon {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idHistorial;
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="idBicho")
	Bicho bichoCampeon;
	@ManyToOne
	Dojo dojo;
    Date fechaInicio;
	Date fechaFin;
	
	public HistorialCampeon(Bicho bichoCamp, Dojo dojo) {
		bichoCampeon = bichoCamp;
		fechaInicio = new Date();
		this.dojo = dojo;
	}
	
	public HistorialCampeon() {
	}
	
	
	public void setFechaFin(){
		fechaFin = new Date();
	}

	public Bicho getBichoCampeon() {
		return bichoCampeon;
	}

	public void setBichoCampeon(Bicho bichoCampeon) {
		this.bichoCampeon = bichoCampeon;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
}
