package ar.edu.unq.epers.bichomon.backend.model.resultadoCombate;

import java.util.List;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public interface Resultado {
	
	public Bicho getGanador();

	public List<Ronda> getDetalleDeLaPelea();

	public Bicho getPerdedor(); 
}
