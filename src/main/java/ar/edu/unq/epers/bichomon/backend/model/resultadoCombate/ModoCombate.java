package ar.edu.unq.epers.bichomon.backend.model.resultadoCombate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;

public class ModoCombate {
	
	Random random; 
	
	public ModoCombate(){
		this.random = new Random(); 
	}
	
	public void setRandom(Random random) {
		this.random = random;
	}

	public ResultadoCombate combatir(Bicho bichoRetador, Bicho bichoCampeon) {

		List<Bicho> peleadores = new ArrayList<Bicho>();
		List<Double> danhosAcumulados = new ArrayList<Double>();
		danhosAcumulados.add(0.0);
		danhosAcumulados.add(0.0);		
		peleadores.add(bichoRetador);
		peleadores.add(bichoCampeon);
		List<Ronda> detallePelea = new ArrayList<Ronda>();
		int rondas = 0;
		int p = 0;
		int r = 1;
		while(rondas < 10){			
			Bicho atacante = peleadores.get(p);
			Bicho atacado = peleadores.get(r);			
			double danhoAcumulado = danhosAcumulados.get(p);
			double danhoEnRonda = this.calcularDmg(atacante);			
			danhoAcumulado = danhoAcumulado + danhoEnRonda;
			danhosAcumulados.set(p, danhoAcumulado);			
			Ronda nuevaRonda = new Ronda(atacante, danhoEnRonda, atacado); 
			detallePelea.add(nuevaRonda);			
			if(danhoAcumulado > atacado.getEnergia()){
				return new ResultadoCombate(atacante, atacado, detallePelea);
			}
			p = (p + 1) % 2;
			r = (r + 1) % 2;
			rondas++;
		}
		return new ResultadoCombate(bichoCampeon, bichoRetador, detallePelea);
	}

	public double calcularDmg(Bicho pokePegador) {		
		Integer energia = pokePegador.getEnergia();
		double random = 0.5 + (this.random.nextDouble() * 0.5);
		double danho = energia * random;
		return danho;
	}
}
