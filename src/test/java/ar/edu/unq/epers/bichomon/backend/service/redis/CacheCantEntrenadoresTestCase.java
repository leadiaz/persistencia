package ar.edu.unq.epers.bichomon.backend.service.redis;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;

public class CacheCantEntrenadoresTestCase {
	private  CacheCantEntrenadores cache; 
	private Entrenador entrenador1;
	private Entrenador entrenador2;
	private Entrenador entrenador3;
	private Entrenador entrenador4;
	private Entrenador entrenador5;
	private Entrenador entrenador6;
	private Ubicacion ubi1;
	private Ubicacion ubi2;
	 @Before
	    public void setup() {
		 	this.cache = new CacheCantEntrenadores();
	        this.entrenador1 = new Entrenador("Pablo Perez");
	        this.entrenador2 = new Entrenador("Ricardo Centurion");
	        this.entrenador3 = new Entrenador("Wilmar Barrios");
	        this.entrenador4 = new Entrenador("Frank Fabra ");
	        this.entrenador5 = new Entrenador("Dario Benedetto");
	        this.entrenador6 = new Entrenador("Cristian Pavon");
	        
	        ubi1 = new Pueblo("La Boca");
	        this.cache.put(ubi1, Arrays.asList(this.entrenador1, this.entrenador2, this.entrenador4, this.entrenador5));
	        ubi2 = new Guarderia("Quilmes");
	        this.cache.put(ubi2, Arrays.asList(this.entrenador1, this.entrenador2, this.entrenador3, this.entrenador6));    
	 }
	 
	 @Test
	 public void obtenerBusqueda() {
	        List<Entrenador> busqueda = this.cache.get(ubi1.getNombreUbicacion());
	        Assert.assertEquals(busqueda.get(0).getNombre(), entrenador1.getNombre());
	        Assert.assertEquals(busqueda.get(1).getNombre(), entrenador2.getNombre());
	        Assert.assertEquals(busqueda.get(2).getNombre(), entrenador4.getNombre());
	        Assert.assertEquals(busqueda.get(3).getNombre(), entrenador5.getNombre());
	    }
	 @Test
	 public void obtenerBusqueda2() {
	        List<Entrenador> busqueda = this.cache.get(ubi2.getNombreUbicacion());
	        Assert.assertEquals(busqueda.get(0).getNombre(), entrenador1.getNombre());
	        Assert.assertEquals(busqueda.get(1).getNombre(), entrenador2.getNombre());
	        Assert.assertEquals(busqueda.get(2).getNombre(), entrenador3.getNombre());
	        Assert.assertEquals(busqueda.get(3).getNombre(), entrenador6.getNombre());
	   }
	 @Test
	 public void testSize() {
	        Assert.assertEquals(2, this.cache.size());
	    }
	 @Test
	    public void busquedaVacia() {
		 	Ubicacion ubi3 = new Pueblo("Sochi");
	        List<Entrenador> busqueda = this.cache.get( ubi3.getNombreUbicacion());
	        Assert.assertNull(busqueda);
	    }
	 @After
	 public void cleanup() {
	        this.cache.clear();
	    }
}
