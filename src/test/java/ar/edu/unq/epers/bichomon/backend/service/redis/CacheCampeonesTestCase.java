package ar.edu.unq.epers.bichomon.backend.service.redis;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;

public class CacheCampeonesTestCase {
	private  CacheCampeones cache; 
	private Entrenador entrenador1;
	private Entrenador entrenador2;
	private Dojo doj1;
	private Dojo doj2;
	 @Before
	    public void setup() {
		 	this.cache = new CacheCampeones();
	        this.entrenador1 = new Entrenador("Wilmar Barrios");
	        this.entrenador2 = new Entrenador("Ricardo Centurion");
	        
	        this.doj1 = new Dojo("Rio Do Janeiro Kappoeira");
	        this.cache.put(doj1,this.entrenador1);
	        this.doj2 = new Dojo("Quilmes Fight");
	        this.cache.put(doj2,this.entrenador2);    
	 }
	 
	 @Test
	 public void obtenerBusqueda() {
	        Entrenador busqueda = this.cache.get(doj1);
	        Assert.assertEquals(this.entrenador1.getNombre(), busqueda.getNombre());
	    }
	 @Test
	 public void obtenerBusqueda2() {
	        Entrenador busqueda = this.cache.get(doj2);
	        Assert.assertEquals(this.entrenador2.getNombre(), busqueda.getNombre());
	   }
	 @Test
	 public void testSize() {
	        Assert.assertEquals(2, this.cache.size());
	    }
	 @Test
	    public void busquedaVacia() {
		 	Dojo doj3 = new Dojo("Sochi");
	        Entrenador busqueda = this.cache.get(doj3);
	        Assert.assertNull(busqueda);
	    }
	 
	 @Test
	    public void busquedaDeTodosLosEntrenadores() {
		 List<Entrenador> busqueda = this.cache.getAll();
	        Assert.assertEquals(2,busqueda.size());
	    }
	 
	 @Test
	    public void busquedaDeTodosLosEntrenadores2() {
		 this.cache.clear();
		 List<Entrenador> busqueda = this.cache.getAll();
	        Assert.assertEquals(0,busqueda.size());
	    }
	 
	 @After
	 public void cleanup() {
	        this.cache.clear();
	    }
}
