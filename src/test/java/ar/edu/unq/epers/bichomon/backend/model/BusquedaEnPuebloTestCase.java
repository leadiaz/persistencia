package ar.edu.unq.epers.bichomon.backend.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;

public class BusquedaEnPuebloTestCase {

	Pueblo pueblo = new Pueblo("Pueblo");
	@Mock
	Random random;
	
	@Before
	public void setUp() throws Exception {
		Especie raichu = new Especie("raichu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 50, 0);
		Especie charmander = new Especie("charmander", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 50, 0);
		Especie newchu = new Especie("newchu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 50, 0);
		Especie pikachu = new Especie("pikachu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		pueblo = new Pueblo("Pueblo");
		pueblo.agregarEspecie(pikachu, 50);
		pueblo.agregarEspecie(newchu, 1);
		pueblo.agregarEspecie(raichu, 100);
		pueblo.agregarEspecie(charmander, 80);
	}

	@Test
	public void cuandoSeAgregaUnaEspecieAlPuebloSeOrdenaPorProbabilidad() {
		assertEquals(pueblo.getEspeciesEnPueblo().get(0).getProbabilidadDeSerEncontrado(), (Integer)1);
		assertEquals(pueblo.getEspeciesEnPueblo().get(1).getProbabilidadDeSerEncontrado(), (Integer)50);
		assertEquals(pueblo.getEspeciesEnPueblo().get(2).getProbabilidadDeSerEncontrado(), (Integer)80);
		assertEquals(pueblo.getEspeciesEnPueblo().get(3).getProbabilidadDeSerEncontrado(), (Integer)100);
	}
	
	@Test
	public void cuandoSeBuscaUnBichoYLaProbabilidadEsDe75SeEncuentraUnCharmander() {
		random = Mockito.mock(Random.class);
		Mockito.when(random.nextInt(100)).thenReturn(75);
		pueblo.setRandom(random);
		Especie encontrado = pueblo.buscarEspecie();
		assertEquals(encontrado.getNombre(), "charmander");
	}
	
	@Test
	public void cuandoSeBuscaUnBichoYLaProbabilidadEsDe1SeEncuentraUnNewchu() {
		random = Mockito.mock(Random.class);
		Mockito.when(random.nextInt(100)).thenReturn(1);
		pueblo.setRandom(random);
		Especie encontrado = pueblo.buscarEspecie();
		assertEquals(encontrado.getNombre(), "newchu");
	}
	
	@Test
	public void cuandoSeBuscaUnBichoYLaProbabilidadEsDe90SeEncuentraUnRaichu() {
		random = Mockito.mock(Random.class);
		Mockito.when(random.nextInt(100)).thenReturn(90);
		pueblo.setRandom(random);
		Especie encontrado = pueblo.buscarEspecie();
		assertEquals(encontrado.getNombre(), "raichu");
	}
}
