package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionIncorrectaException;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;

public class SubirNivelTestCase {

	DataHibernateDao dao;
	BichoServiceImp bichoService;
	DataServiceImp service;
	EspecieServiceImp especieService;
	@Mock
	FactorBusqueda factorBusqueda;
	
	@Before
	public void setUp() throws Exception {

		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);	
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
	}
	
	@Test
	public void entrenadorRetaUnDojoTiene90ExpYSubeNivel() {	
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);	
		Dojo dojo = service.recuperar(Dojo.class, 2);
		entrenador.setUbicacionActual(dojo);
		entrenador.setExperiencia(90);
		Bicho bicho1 = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho1);
		service.update(entrenador);
		bichoService.duelo(entrenador.getNombre(), 1);
		Entrenador juan = service.recuperar(Entrenador.class, 1);
		assertEquals(juan.getExperiencia(), 100);
		assertEquals(juan.getBichosCapturados().size(), 1);
		assertEquals(juan.getNivel(), 2);
	}

	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}
}
