package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.NoPuedeEvolucionarException;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;



public class PuedeEvolucionarTestCase extends Object {

	DataHibernateDao dao;
	BichoServiceImp bichoService;
	EspecieServiceImp especieService;
	DataServiceImp service;
	Bicho bicho;
	@Mock
	FactorBusqueda factorBusqueda;
	
	@Before
	public void setUp() throws Exception {
		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);	
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
		bicho = especieService.crearBicho("pikachu");
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		entrenador.agregarBicho(bicho);
		bicho.setFechaCapura(LocalDateTime.of(2016, 6, 2, 2, 40));
		service.update(entrenador);
	}
	
	@Test
	public void bichoEvolucionarPikachuEnRaichuPorEdad(){		
		bichoService.evolucionar("Juan", 1);
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		Bicho evolucion = entrenador.getBicho(1);
		assertEquals(evolucion.getEspecie().getNombre(), "raichu");
	}
	
	
	@Test(expected = NoPuedeEvolucionarException.class)
	public void cuandoUnEntrenadorQuiereEvolucionarARaichuTieneQueSerNivel4(){
		bichoService.evolucionar("Juan", 1);
		bichoService.evolucionar("Juan", 1);
	}
	
	@Test(expected = NoPuedeEvolucionarException.class)
	public void bichoEvolucionarTresVecesPikachuEnNewchuPeroSoloPuedeDosYaQueTieneDosEvoluciones(){
		bicho.setFechaCaptura(LocalDateTime.of(2010, 6, 2, 2, 40));
		bichoService.evolucionar("Juan", 1);
		bichoService.evolucionar("Juan", 1);
		bichoService.evolucionar("Juan", 1);
	}
	
	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}
}
