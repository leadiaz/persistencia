package ar.edu.unq.epers.bichomon.backend.service.mapa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.HistorialCampeon;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCantEntrenadores;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class MapaServiceTestCase {
	DataServiceImp service;
	MapaServiceImp mapa;
	EspecieServiceImp especieService;
	DataHibernateDao daoH;
	
	
	@Before
	public void setUp() throws Exception
	{
		daoH = new DataHibernateDao();
		service = new DataServiceImp(daoH);
		service.crearSetDatosIniciales();
		mapa = new MapaServiceImp(daoH ,new UbicacionDao(), new EventoDAO(), new CacheCantEntrenadores());		
		especieService = new EspecieServiceImp(daoH);
	}
	
	
	@Test
	public void TestcantidadEntrenadoresEnElLugarActual(){
		assertEquals(1,mapa.cantidadEntrenadores("paleta"));
	}
	
	
	@Test
	public void TestElBichoCampeonDeUnDojoLlamadoCobra(){
		Dojo dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		Entrenador entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bichoCamp1 = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bichoCamp1);
	    dojo.setBichoCampeon(bichoCamp1);
	    service.update(entrenador);
	    service.update(dojo);
	    assertEquals("pikachu",mapa.campeon("Cobra").getEspecie().getNombre());
	}
	
	@Test
	public void TestElCampeonHistoricoSobreDosCampeones(){
		Dojo dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		Entrenador entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		
		Entrenador ash = new Entrenador("Ash");

		List <HistorialCampeon> historial = new ArrayList<HistorialCampeon>();
		Bicho raichu = especieService.crearBicho("Raichu");
		Bicho pikachu = especieService.crearBicho("Pikachu");
		HistorialCampeon camp1= new HistorialCampeon(raichu, dojo);
		HistorialCampeon camp2 = new HistorialCampeon(pikachu, dojo);
		
		camp1.setFechaInicio(new Date(115, 10, 5));
		camp1.setFechaFin(new Date(116,1,1));
		
		camp2.setFechaInicio(new Date(116,1,1));
		historial.add(camp1);
		historial.add(camp2);
		
		entrenador.agregarBicho(raichu);
		ash.agregarBicho(pikachu);
	    dojo.setBichoCampeon(pikachu);
	    dojo.setHistorialCampeones(historial);
	    service.guardar(ash);
	    service.update(entrenador);
	    service.update(dojo);
	    
		assertEquals("Ash",mapa.campeonHistorico("Cobra").getOwner().getNombre());
	}
	
	
	@After
	public void cleanup() {
		service.eliminarDatos();
	}	
}
