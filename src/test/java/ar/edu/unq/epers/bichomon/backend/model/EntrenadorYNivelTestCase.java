package ar.edu.unq.epers.bichomon.backend.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Nivel;

public class EntrenadorYNivelTestCase {

	Entrenador entrenador;
	Nivel nivel;
	
	@Before
	public void setUp() throws Exception {
		List<Integer> limites = new ArrayList<Integer>(Arrays.asList(99, 400, 1000, 2000, 3000, 4000, 5000, 6000,7000));
		nivel = new Nivel(limites); 
		entrenador = new Entrenador("Juan");
	}

	@Test
	public void cuandoSeCreaUnNuevoEntrenadorEsteTieneNivel1() {
		entrenador.setNivel(nivel.getNivel(entrenador.getExperiencia()));
		assertEquals(entrenador.getNivel(), 1);
	}
	
	@Test
	public void cuandoUnEntrenadorPasaLos99PuntosDeExperienciaSuNivelSeActualizaA2() {
		entrenador.setExperiencia(100);
		entrenador.setNivel(nivel.getNivel(entrenador.getExperiencia()));
		assertEquals(entrenador.getNivel(), 2);
	}

	@Test
	public void cuandoUnEntrenadorPasaLos400PuntosDeExperienciaSuNivelSeActualizaA3() {
		entrenador.setExperiencia(500);
		entrenador.setNivel(nivel.getNivel(entrenador.getExperiencia()));
		assertEquals(entrenador.getNivel(), 3);
	}
	
	@Test
	public void cuandoUnEntrenadorPasaLos7000PuntosDeExperienciaSuNivelSeActualizaA10() {
		entrenador.setExperiencia(20000);
		entrenador.setNivel(nivel.getNivel(entrenador.getExperiencia()));
		assertEquals(entrenador.getNivel(), 10);
	}
}
