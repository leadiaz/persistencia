package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionIncorrectaException;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;

public class AbandonarYBuscarEnGuarderiaTestCase {

	DataHibernateDao dao;
	BichoServiceImp bichoService;
	EspecieServiceImp especieService;
	DataServiceImp service;
	Guarderia guarderia;
	Bicho bicho1;
	Bicho bicho2;
	@Mock
	FactorBusqueda factorBusqueda;
	
	@Before
	public void setUp() throws Exception {

		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);
		
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		this.service.eliminarDatos();
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
		guarderia = new Guarderia("Pepe");
		service.guardar(guarderia);
		
		bicho1 = especieService.crearBicho("pikachu");
		bicho2 = especieService.crearBicho("raichu");
	}
	
	public void setup2(){
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		entrenador.agregarBicho(bicho1);	
		entrenador.agregarBicho(bicho2);
		entrenador.setUbicacionActual(guarderia);
		Entrenador entrenador2 = new Entrenador("Ash");
		entrenador2.setUbicacionActual(guarderia);
		service.update(entrenador);
		service.guardar(entrenador2);
		bichoService.abandonar("Juan", 1);	
	}

	@Test(expected = NoHayBichosException.class)
	public void cuandoUnEntrenadorQuiereAbandonarSuBichoUltimoBichoNoPodraHacerlo() {	
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		System.out.println(bicho1.getId());
		entrenador.agregarBicho(bicho1);	
		service.update(entrenador);
		bichoService.abandonar("Juan", 1);		
	}
	
	@Test(expected = UbicacionIncorrectaException.class)
	public void cuandoUnEntrenadorQuiereAbandonarEnUnPuebloNoPodraHacerlo() {	
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		entrenador.agregarBicho(bicho1);	
		entrenador.agregarBicho(bicho2);	
		service.update(entrenador);
		bichoService.abandonar("Juan", 1);		
	}
	
	@Test
	public void cuandoUnEntrenadorAbandonaUnBichoEnUnaGuarderiaElBichoLoVaASumarComoExEntrenador() {	
		this.setup2();
		Entrenador entrenador1 = service.recuperar(Entrenador.class, 1);
		Guarderia guarderia = service.recuperar(Guarderia.class, 3);
		assertEquals(entrenador1.getBichosCapturados().size(), 1);
		assertEquals(guarderia.getNombreUbicacion(), "Pepe");
		Bicho abandonado = guarderia.getBichosAbandonados().get(0);
		assertEquals(guarderia.getBichosAbandonados().size(), 1);
		assertEquals(abandonado.getId(), 1);
		assertEquals(abandonado.getExEntrenadores().get(0).getNombre(), "Juan");
	}	
	
	@Test
	public void cuandoUnEntrenadorAbandonaUnBichoEnUnaGuarderiaOtroEntranadorLoPuedeCapturar() {	
	    this.setup2();
		bichoService.buscar("Ash");	
		Entrenador ash = service.recuperar(Entrenador.class, 1);		
		Guarderia guarderia = service.recuperar(Guarderia.class, 3);
		assertEquals(ash.getBichosCapturados().size(), 1);
		assertEquals(guarderia.getBichosAbandonados().size(), 0);
	}
	
	@Test(expected = NoHayBichosException.class)
	public void cuandoUnBichoEsAbandonadoNoPuedeSerCapuradoPoSuExExtrenador() {	
	    this.setup2();
		bichoService.buscar("Juan");	
	}
	
	@Test
	public void cuandoUnEntrenadorAbandonaUnBichoEnUnaGuarderiaElBichoLoVaASumarComoExEntrenador_2() {	
	    this.setup2();
	    Bicho bicho = especieService.crearBicho("pikachu");
	    Entrenador entrenador = service.recuperar(Entrenador.class, 2);
	    entrenador.agregarBicho(bicho);
	    service.update(entrenador);	    
		bichoService.buscar("Ash");	
		bichoService.abandonar("Ash", 1);			
		Guarderia guarderia = service.recuperar(Guarderia.class, 3);
		Bicho abandonado = guarderia.getBichosAbandonados().get(0);
		assertEquals(abandonado.getExEntrenadores().get(0).getNombre(), "Juan");
		assertEquals(abandonado.getExEntrenadores().get(1).getNombre(), "Ash");
	}
	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}
}
