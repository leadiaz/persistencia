package ar.edu.unq.epers.bichomon.backend.service;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoArribo;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceMapa;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.feedService.impl.FeedServiceImpl;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCantEntrenadores;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class FeedServiceTestCase {
		EventoDAO dao;
		Evento eventoOne;
		Evento eventoTwo;
		FeedServiceImpl  feed;
		DataHibernateDao daoH;
		DataServiceMapa service;
		MapaServiceImp mapaService;
	
	@Before
	public void setUp() throws Exception{
		dao = new EventoDAO();
		Date fecha1 = new Date();
		fecha1.setYear(2013);
		Date fecha2 = new Date();
		fecha2.setYear(2011);
		Date fecha3 = new Date();
		fecha3.setYear(2016);
		daoH = new DataHibernateDao();
		eventoOne = new EventoArribo("Fernandinho","Socotroco");   
		eventoOne.setFechaInicioEvento(fecha1);
		eventoTwo = new EventoArribo("Fernandinho","Sacawea");
		eventoTwo.setFechaInicioEvento(fecha2);
		Evento eventoTree = new EventoArribo("Fernandinho","Sacawea");
		eventoTree.setFechaInicioEvento(fecha3);
		this.dao.save(eventoOne);  
		this.dao.save(eventoTwo); 
		this.dao.save(eventoTree);
		UbicacionDao daoU = new UbicacionDao();
		feed = new FeedServiceImpl(dao, new DataServiceImp(daoH) , daoU); 
		
	    mapaService = new MapaServiceImp(daoH, daoU, dao, new CacheCantEntrenadores());
		service = new DataServiceMapa(mapaService);	
		service.crearSetDatosIniciales();
	}
	
	@After
	public void dropAll() {
		dao.deleteAll();
		service.eliminarDatos();
	}	
	
	@Test
	public void TestObtenerLosEventosDeUnDeterminadoEntrenador(){
		List<Evento>  eventos = this.feed.feedEntrenador("Fernandinho");
		Assert.assertEquals(3, eventos.size());
		Assert.assertEquals(2016, eventos.get(0).getFechaInicioEvento().getYear());
		Assert.assertEquals(2013, eventos.get(1).getFechaInicioEvento().getYear());
		Assert.assertEquals(2011, eventos.get(2).getFechaInicioEvento().getYear());		
	}
	
	@Test
	public void TestObtenerLosEventosDeUnDeterminadoLugar(){
		mapaService.mover("ash", "Agualand");	
		mapaService.mover("ash", "Lagartoland");	
		List<Evento>  eventos = this.feed.feedUbicacion("ash");
		Assert.assertEquals(2, eventos.size());
		Assert.assertEquals("Lagartoland", eventos.get(0).getNombreUbicacion());
		Assert.assertEquals("Agualand", eventos.get(1).getNombreUbicacion());		
	}
}
