package ar.edu.unq.epers.bichomon.backend.service.redis;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacion;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacionDefault;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceMapa;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.feedService.impl.FeedServiceImpl;
import ar.edu.unq.epers.bichomon.backend.service.leaderboardService.impl.LeaderboardServiceImpl;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class CacheDueloTestCase {
	
	BichoServiceImp bichoService;
	DataServiceMapa service;
	MapaServiceImp mapaService;
	DataServiceImp serviceData;
	EventoDAO daoEvento;
	EspecieServiceImp especieService;
	DataHibernateDao hibernateDao;
	LeaderboardServiceImpl leaderService;
	CacheCampeones cacheCamp;

	@Before
	public void setUp() throws Exception {
		daoEvento = new EventoDAO();
		hibernateDao = new DataHibernateDao();
		serviceData = new DataServiceImp(hibernateDao);		
		service = new DataServiceMapa(mapaService);	
		especieService = new EspecieServiceImp(hibernateDao);
		cacheCamp = new CacheCampeones();
		serviceData.eliminarDatos();
	    cacheCamp.clear();
		serviceData.crearSetDatosIniciales();
		bichoService = new BichoServiceImp(hibernateDao, daoEvento, cacheCamp);
		leaderService = new LeaderboardServiceImpl(hibernateDao, cacheCamp);
	}

	@Test
	public void guardarCampeonEnCache() {
		
		Entrenador entrenador = serviceData.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		Dojo dojo = serviceData.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		entrenador.setUbicacionActual(dojo);
		serviceData.update(entrenador);
		bichoService.duelo("juan", 1);	
		List<Entrenador> campeones = leaderService.campeones();
		assertEquals(campeones.get(0).getNombre(), entrenador.getNombre());
	}
	
	@Test
	public void guardarCampeonEnCache2() {
		
		Entrenador entrenador = serviceData.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		Dojo dojo = serviceData.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		entrenador.setUbicacionActual(dojo);
		serviceData.update(entrenador);
		bichoService.duelo("juan", 1);
		Entrenador ash = new Entrenador("ash");
		Bicho raichu = especieService.crearBicho("raichu");
		ash.agregarBicho(raichu);
		ash.setUbicacionActual(dojo);
		serviceData.guardar(ash);		
		bichoService.duelo("ash", 2);
		
		List<Entrenador> campeones = leaderService.campeones();
		assertEquals(campeones.get(0).getNombre(), ash.getNombre());
			
	}	
	
	 @After
	 public void cleanup() {
		 this.serviceData.eliminarDatos();
	     this.cacheCamp.clear();
	 }
}
