package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionIncorrectaException;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;

public class AbandonarYBuscarEnPueblo {

	DataHibernateDao dao;
	BichoServiceImp bichoService;
	EspecieServiceImp especieService;
	DataServiceImp service;
	Bicho bicho1;
	Bicho bicho2;
	@Mock
	FactorBusqueda factorBusqueda;
	@Before
	public void setUp() throws Exception {

		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);		
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();		
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());	
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
	}

	@Test(expected = NoHayBichosException.class)
	public void cuandoUnPuebloNoTieneEspeciesElEntrenadorNoPuedeEncontrarBichos() {
		bichoService.buscar("Juan");
	}
	
	@Test
	public void cuandoUnPuebloTieneSoloEspeciePikachuUnEntrenadorEncontraraBichosDeEsaEspecie(){
		Especie pikachu = especieService.getEspecie("pikachu");
		Pueblo pueblo = service.recuperar(Pueblo.class, 1);
		pueblo.agregarEspecie(pikachu, 10);
		service.update(pueblo);
		Bicho encontrado = bichoService.buscar("Juan");
		assertEquals(encontrado.getEspecie().getNombre(), "pikachu");
	}
	
	@Test(expected = UbicacionIncorrectaException.class)
	public void unEntrenadorNoPuedeAbandonarUnBichoEnUnPueblo(){
		Especie pikachu = especieService.getEspecie("pikachu");
		Pueblo pueblo = service.recuperar(Pueblo.class, 1);
		pueblo.agregarEspecie(pikachu, 14);
		service.update(pueblo);
		bichoService.buscar("Juan");
		bichoService.buscar("Juan");
		bichoService.abandonar("Juan", 1);
	}
	
	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}
}
