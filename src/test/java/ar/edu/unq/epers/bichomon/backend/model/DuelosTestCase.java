package ar.edu.unq.epers.bichomon.backend.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;


import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.ModoCombate;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Resultado;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.ResultadoCombate;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Ronda;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;

public class DuelosTestCase {

	Entrenador entrenador1;
	Entrenador entrenador2;
	Dojo dojo;
	Bicho bicho1;
	Bicho bicho2;
	ModoCombate combate;
	@Mock
	Random random;
	
	@Before
	public void setUp() throws Exception {
		 
		random = Mockito.mock(Random.class);
		Mockito.when(random.nextDouble()).thenReturn(1.0);
		combate = new ModoCombate();
		combate.setRandom(random);
		dojo = new Dojo("Cobra");
		Especie raichu = new Especie("raichu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 50, 0);
		Especie pikachu = new Especie("pikachu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		
		bicho1 = new Bicho(raichu);
		bicho2 = new Bicho(pikachu);
	}

	@Test
	public void elDanhoCausadoPorElBicho1EsDe50() {
		assertEquals(combate.calcularDmg(bicho1), 50.0, 0);	
	}
	
	@Test
	public void elDanhoCausadoPorElBicho2EsDe10() {
		assertEquals(combate.calcularDmg(bicho2), 10.0, 0);	
	}
	
	@Test
	public void cuandoCombatenElBicho1ConElBicho2GanaElBicho1EnUnaRonda() {
		ResultadoCombate res = combate.combatir(bicho1, bicho2);
		Ronda ronda = res.getDetalleDeLaPelea().get(0);
		
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(res.getPerdedor().getEspecie().getNombre(), "pikachu");
		assertEquals(res.getDetalleDeLaPelea().size(), 1);
		assertEquals(ronda.getAtacante().getEspecie().getNombre(), "raichu");
		assertEquals(ronda.getAtacado().getEspecie().getNombre(), "pikachu");
		assertEquals(ronda.getDanhoAtacante(), 50, 0);		
	}
	
	@Test
	public void cuandoCombatenElBicho1ConElBicho2GanaElBicho1EnDosRondas() {
		ResultadoCombate res = combate.combatir(bicho2, bicho1);
		Ronda ronda1 = res.getDetalleDeLaPelea().get(0);
		Ronda ronda2 = res.getDetalleDeLaPelea().get(1);
		
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(res.getPerdedor().getEspecie().getNombre(), "pikachu");
		assertEquals(res.getDetalleDeLaPelea().size(), 2);
		
		assertEquals(ronda1.getAtacante().getEspecie().getNombre(), "pikachu");
		assertEquals(ronda1.getAtacado().getEspecie().getNombre(), "raichu");
		assertEquals(ronda1.getDanhoAtacante(), 10, 0);		
		
		assertEquals(ronda2.getAtacante().getEspecie().getNombre(), "raichu");
		assertEquals(ronda2.getAtacado().getEspecie().getNombre(), "pikachu");
		assertEquals(ronda2.getDanhoAtacante(), 50, 0);	
	}
	
	@Test
	public void cuandoRetanAUnDojoSinCampeonElRetadorPasaASerElNuevoCampeonDelDojo() {
		assertFalse(dojo.hayCampeon());
		assertEquals(dojo.getHistorialCampeones().size(), 0);		
		Resultado res = dojo.retar(bicho1);
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(dojo.getHistorialCampeones().size(), 1);
	}
	
	@Test
	public void cuandoUnBichoRetaAUnDojoYGanaEstePasaASerElNuevoCampeonDelDojo() {
		dojo.retar(bicho2);			
		Resultado res = dojo.retar(bicho1);
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(dojo.getHistorialCampeones().size(), 2);		
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(res.getPerdedor().getEspecie().getNombre(), "pikachu");
		assertEquals(res.getDetalleDeLaPelea().size(), 1);
	}
}
