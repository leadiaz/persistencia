package ar.edu.unq.epers.bichomon.backend.service.redis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceMapa;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class CacheMoverTestCase {

	DataServiceMapa service;
	MapaServiceImp mapaService;
	DataServiceImp serviceData;
	EventoDAO daoEvento;
	DataHibernateDao hibernateDao;
	CacheCantEntrenadores cache;
	
	@Before
	public void setUp() throws Exception {
		daoEvento = new EventoDAO();
		hibernateDao = new DataHibernateDao();
		serviceData = new DataServiceImp(hibernateDao);
		cache = new CacheCantEntrenadores();
		mapaService = new MapaServiceImp(hibernateDao, new UbicacionDao(), daoEvento, cache);		
		service = new DataServiceMapa(mapaService);
		service.crearSetDatosIniciales();
		this.cache.clear();
	}

	@Test
	public void cuandoUnEntrenadorSeMueveSeActualizaLaCacheDeLaUbicacionDondeSeMovio() {
		mapaService.mover("ash", "Agualand");
		int cant = mapaService.cantidadEntrenadores("Agualand");
		assertEquals(cant, 1);
	}
	
	@Test
	public void cuandoUnEntrenadorSeMueveSeActualizaLaCacheDeLAUbicacionDondeSeMovioYDeLaAnterior() {
		mapaService.mover("ash", "Agualand");
		mapaService.mover("ash", "Plantaland");
		int cant1 = mapaService.cantidadEntrenadores("Agualand");
		int cant2 = mapaService.cantidadEntrenadores("Plantaland");				
		assertEquals(cant1, 0);
		assertEquals(cant2, 1);
	}
	
	@Test
	public void cuandoUnEntrenadorSeMueveSeActualizaLaCacheDeLAUbicacionDondeSeMovio2() {
		Pueblo p = serviceData.recuperarPorColumna(Pueblo.class, "Plantaland", "nombreUbicacion");
		Entrenador e = new Entrenador("Red");
		e.setUbicacionActual(p);
		serviceData.guardar(e);
		mapaService.mover("ash", "Agualand");
		mapaService.mover("Red", "Agualand");
		int cant1 = mapaService.cantidadEntrenadores("Agualand");
		int cant2 = mapaService.cantidadEntrenadores("Plantaland");				
		assertEquals(cant1, 2);
		assertEquals(cant2, 0);
		int cant = cache.get("Plantaland").size();
		assertEquals(cant, 0);
	}
	
	
	
	 @After
	 public void cleanup() {
	     this.cache.clear();
	     serviceData.eliminarDatos();
	 }

}
