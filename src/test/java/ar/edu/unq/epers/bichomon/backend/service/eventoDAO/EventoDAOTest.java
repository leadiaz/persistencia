package ar.edu.unq.epers.bichomon.backend.service.eventoDAO;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoAbandono;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoArribo;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCaptura;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacion;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;

public class EventoDAOTest {
	private EventoDAO dao;
	Evento eventoOne;
	Evento eventoTwo;
	
	@Before
	public void setup() {
		this.dao = new EventoDAO();
		eventoOne = new EventoArribo("Fernandinho","Socotroco");
		eventoOne.setFechaInicioEvento(new Date());
		eventoTwo = new EventoArribo("Fernandinho","Sacawea");
		eventoTwo.setFechaInicioEvento(new Date());
		Evento eventoTree = new EventoArribo("Javiersinho","Sacawea");
		eventoTree.setFechaInicioEvento(new Date());
		this.dao.save(eventoOne);
		this.dao.save(eventoTwo);
		this.dao.save(eventoTree);
	}
	
	@After
	public void dropAll() {
		this.dao.deleteAll();
	}
	@Test
	public void test_save_and_get_by_id() {
		Evento evento = new EventoArribo("Ash","Paleta");
		this.dao.save(evento);
	
		EventoArribo evento1 = (EventoArribo) this.dao.get(evento.getId());
		Assert.assertEquals("Ash", evento1.getNombreEntrenador());
		Assert.assertEquals("Paleta", evento1.getNombreUbicacion());
		
		// Evento de captura
		
		EventoCaptura eventoCap1 = new EventoCaptura("Ash","Paleta","Pikachu");
		this.dao.save(eventoCap1);
		
		EventoCaptura evento2 = (EventoCaptura) this.dao.get(eventoCap1.getId());
		Assert.assertEquals("Ash", evento2.getNombreEntrenador());
		Assert.assertEquals("Paleta", evento2.getNombreUbicacion());
		Assert.assertEquals("Pikachu", evento2.getNombreEspecieDelBichoCapturado());
		
		// Evento de Abandono
		
		EventoAbandono eventoAb1 = new EventoAbandono("Ash","Paleta","Raichu");
		this.dao.save(eventoAb1);
		
		EventoAbandono evento3 = (EventoAbandono)this.dao.get(eventoAb1.getId());

		Assert.assertEquals("Ash", evento3.getNombreEntrenador());
		Assert.assertEquals("Paleta", evento3.getNombreUbicacion());
		Assert.assertEquals("Raichu", evento3.getNombreEspecieDelBichoAbandonado());
		
		// Evento De Coronacion 
		EventoCoronacion eventoCor1 = new EventoCoronacion("Ash","Paleta","Carlos Araujo IV");
		this.dao.save(eventoCor1);
		
		Assert.assertNotNull(eventoAb1.getId());
		
		EventoCoronacion evento4 = (EventoCoronacion)this.dao.get(eventoCor1.getId());
		Assert.assertEquals("Ash", evento4.getNombreEntrenador());
		Assert.assertEquals("Paleta", evento4.getNombreUbicacion());
		Assert.assertEquals("Carlos Araujo IV", evento4.getNombreEntrenadorDestronado());
	}
	
	@Test
	public void test_get_by_nombreEntrenador() {
		List<Evento>  eventos = this.dao.getByNombreEntrenador(eventoOne.getNombreEntrenador());
		Assert.assertEquals(2, eventos.size());
		
		Evento evento1 = eventos.get(0);
		Assert.assertEquals("Fernandinho", evento1.getNombreEntrenador());
		Assert.assertEquals("Socotroco", evento1.getNombreUbicacion());
	}
	
	@Test
	public void test_get_by_nombreUbicacion() {
		List<Evento>  eventos = this.dao.getByNombreUbicacion(eventoOne.getNombreUbicacion());
		Assert.assertEquals(1, eventos.size());
		
		Evento evento1 = eventos.get(0);
		Assert.assertEquals("Fernandinho", evento1.getNombreEntrenador());
		Assert.assertEquals("Socotroco", evento1.getNombreUbicacion());
	}
}
