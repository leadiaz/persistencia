package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Experiencia;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Nivel;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;

public class DataServiceTestCase {
	
	Entrenador entrenador;
	Bicho pikachu;
	DataServiceImp service;
	Nivel nivel; 
	Experiencia experiencia;
	Pueblo pueblo;
	@Before
	public void setUp() throws Exception {
		service = new DataServiceImp(new DataHibernateDao());
		List<Integer> limites = new ArrayList<Integer>(Arrays.asList(99, 400, 1000, 2000, 3000, 4000, 5000, 6000,7000));
		nivel = new Nivel(limites); 
		experiencia = new Experiencia(10, 10, 10);
		pueblo = new Pueblo("paleta");
		entrenador = new Entrenador("Juan");
		entrenador.setUbicacionActual(pueblo);
	}

	
	@Test
	public void testGuardarEntrenadorYRecuperar() throws Exception {
		Especie electrico = new Especie("pikachu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		service.guardar(electrico);
		pikachu = new Bicho(electrico);
		entrenador.agregarBicho(pikachu);
		this.service.guardar(this.pueblo);
		
		Entrenador juan = this.service.recuperar(Entrenador.class, 1);
		assertEquals(juan.getNombre(), "Juan");
		assertEquals(juan.getExperiencia(), 0);
		assertEquals(juan.getNivel(), 1);
		assertEquals(juan.getBichosCapturados().size(), 1);
		
		Bicho capturado = juan.getBicho(1);
		assertEquals(capturado.getId(), 1);
		assertEquals(capturado.getEnergia(), 10);
		
		Ubicacion ubicacion = juan.getUbicacionActual();
		assertEquals(ubicacion.getNombreUbicacion(), "paleta");
		assertEquals(ubicacion.getEntrenadoresEnElLugar().size(), 1);
	}
	
	@Test
	public void testGuardarNivelYRecuperar() {
		this.service.guardar(this.nivel);
		Nivel nivel_guardado = this.service.recuperar(Nivel.class, 1);
		assertEquals(nivel_guardado.getLimites().size(), 9);
		assertEquals(nivel_guardado.getLimites().get(0), (Integer)99);
		assertEquals(nivel_guardado.getLimites().get(1), (Integer)400);
	}
	
	@Test
	public void testGuardarExperienciaYRecuperar() {
		this.service.guardar(this.experiencia);
		Experiencia exp = this.service.recuperar(Experiencia.class, 1);
		assertEquals(exp.getCapturar(), (Integer)10);
		assertEquals(exp.getCombatir(), (Integer)10);
		assertEquals(exp.getEvolucionarBicho(), (Integer)10);
	}
	
	@Test
	public void testGuardarGuarderiaYRecuperar() {
		Guarderia guarderiaPrueba = new Guarderia("guarderia");
		service.guardar(guarderiaPrueba);
		Guarderia guarderia = service.recuperar(Guarderia.class, 1);
		assertEquals(guarderia.getNombreUbicacion(), "guarderia");
	}
	
	@Test
	public void testRecuperar() {
		this.service.guardar(this.pueblo);
		Entrenador e = service.recuperarPorColumna(Entrenador.class, "Juan", "nombre");
		assertEquals(e.getNombre(), "Juan");
	}
	
	@After
	public void cleanup() {
		this.service.eliminarDatos();
	}
}
