package ar.edu.unq.epers.bichomon.backend.service.eventoDAO;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.evento.Evento;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoAbandono;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCaptura;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacion;
import ar.edu.unq.epers.bichomon.backend.model.evento.EventoCoronacionDefault;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Pueblo;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceMapa;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.feedService.impl.FeedServiceImpl;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCantEntrenadores;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class RegistrarEventoTest {

	BichoServiceImp bichoService;
	DataServiceMapa service;
	MapaServiceImp mapaService;
	DataServiceImp serviceData;
	FeedServiceImpl  feedService;
	EventoDAO daoEvento;
	EspecieServiceImp especieService;
	private DataHibernateDao hibernateDao;
	
	@Before
	public void setUp() throws Exception {
		daoEvento = new EventoDAO();
		hibernateDao = new DataHibernateDao();
		serviceData = new DataServiceImp(hibernateDao);
		this.feedService = new FeedServiceImpl(daoEvento, serviceData, new UbicacionDao());
		daoEvento.deleteAll();
		hibernateDao.eliminarDatos();
		serviceData.eliminarDatos();
		mapaService = new MapaServiceImp(hibernateDao, new UbicacionDao(), daoEvento, new CacheCantEntrenadores());		
		service = new DataServiceMapa(mapaService);	
		especieService = new EspecieServiceImp(hibernateDao);
		FactorBusqueda factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);	
		serviceData.crearSetDatosIniciales();
		service.crearSetDatosIniciales();
		bichoService = new BichoServiceImp(hibernateDao, daoEvento, new CacheCampeones());
		bichoService.SetFactorBusqueda(factorBusqueda);
	}

	@Test
	public void registrarEventoDeArriboTest() {
		mapaService.mover("ash", "Agualand");
		List<Evento> eventos = this.feedService.feedEntrenador("ash");
		assertEquals(eventos.size(), 1);
		Evento evento = eventos.get(0);
		assertEquals("ash", evento.getNombreEntrenador());
		assertEquals("Agualand", evento.getNombreUbicacion());
	}
	
	@Test
	public void registrarEventoDeCapturaTest() {
		Especie pikachu = especieService.getEspecie("pikachu");
		Pueblo pueblo = serviceData.recuperarPorColumna(Pueblo.class, "Plantaland", "nombreUbicacion");
		pueblo.agregarEspecie(pikachu, 14);
		serviceData.update(pueblo);
		bichoService.buscar("ash");
		
		List<Evento> eventos = this.feedService.feedEntrenador("ash");
		assertEquals(eventos.size(), 1);
		EventoCaptura evento = (EventoCaptura)eventos.get(0);
		assertEquals("ash", evento.getNombreEntrenador());
		assertEquals("Plantaland", evento.getNombreUbicacion());
		assertEquals("pikachu", evento.getNombreEspecieDelBichoCapturado());
		
	}
	
	@Test
	public void registrarEventoDeAbandonarTest() {
		
		Bicho elBicho = especieService.crearBicho("pikachu");
		Bicho humo = especieService.crearBicho("pikachu");
		Entrenador e = serviceData.recuperarPorColumna(Entrenador.class, "Juan", "nombre");
		Guarderia guarderia = new Guarderia("St. b");
		serviceData.guardar(guarderia);
		e.agregarBicho(elBicho);
		e.agregarBicho(humo);
		e.setUbicacionActual(guarderia);
		serviceData.update(e);
		bichoService.abandonar("Juan", elBicho.getId());
		List<Evento> eventos = this.feedService.feedEntrenador("Juan");
		assertEquals(eventos.size(), 1);
		EventoAbandono evento = (EventoAbandono) eventos.get(0);
		assertEquals("Juan", evento.getNombreEntrenador());
		assertEquals("St. b", evento.getNombreUbicacion());
		assertEquals("pikachu", evento.getNombreEspecieDelBichoAbandonado());
	}
	
	@Test
	public void registrarEventoDeCoronacionDefaultTest() {
		
		Entrenador entrenador = serviceData.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		Dojo dojo = serviceData.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		entrenador.setUbicacionActual(dojo);
		serviceData.update(entrenador);
		bichoService.duelo("juan", 1);
		
		List<Evento> eventos = this.feedService.feedEntrenador("Juan");
		assertEquals(eventos.size(), 1);
		EventoCoronacionDefault evento = (EventoCoronacionDefault) eventos.get(0);
		assertEquals("Juan", evento.getNombreEntrenador());
		assertEquals("Cobra", evento.getNombreUbicacion());	
	}
	
	@Test
	public void registrarEventoDeCoronacionTest() {
		
		Entrenador entrenador = serviceData.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		Dojo dojo = serviceData.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		entrenador.setUbicacionActual(dojo);
		serviceData.update(entrenador);
		bichoService.duelo("juan", 1);
		Entrenador ash = serviceData.recuperarPorColumna(Entrenador.class, "ash", "nombre");
		Bicho raichu = especieService.crearBicho("raichu");
		ash.agregarBicho(raichu);
		ash.setUbicacionActual(dojo);
		serviceData.update(ash);		
		bichoService.duelo("ash", 2);
		
		List<Evento> eventos = this.feedService.feedEntrenador("ash");
		assertEquals(eventos.size(), 1);
		EventoCoronacion evento = (EventoCoronacion) eventos.get(0);
		assertEquals("ash", evento.getNombreEntrenador());
		assertEquals("Cobra", evento.getNombreUbicacion());	
		assertEquals("Juan", evento.getNombreEntrenadorDestronado());	
	}	

	@After
	public void dropAll() {
		this.serviceData.eliminarDatos();
		this.daoEvento.deleteAll();
	}
}
