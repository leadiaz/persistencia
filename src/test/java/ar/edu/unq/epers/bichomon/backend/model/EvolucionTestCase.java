package ar.edu.unq.epers.bichomon.backend.model;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.Evoluciones;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
public class EvolucionTestCase {

	Entrenador entrenador;
	Bicho bicho;
	Especie pikachu;
	Especie raichu;
	Especie newchu;
	
	@Before
	public void setUp() throws Exception {
		
		entrenador = new Entrenador("Red");
		Evoluciones tabla1 = new Evoluciones(100, 1, LocalDateTime.of(2017, 1, 20, 4, 2), 2);
		Evoluciones tabla2 = new Evoluciones(400, 4, LocalDateTime.of(2017, 1, 20, 4, 2), 4);
		newchu = new Especie("newchu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		raichu = new Especie("raichu", newchu, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);		
		pikachu = new Especie("pikachu", raichu, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0);
		pikachu.setTablaEvoluciones(tabla1);
		raichu.setTablaEvoluciones(tabla2);
		raichu.setRaiz(pikachu);
		newchu.setRaiz(pikachu);
		bicho = new Bicho(pikachu);
		entrenador.agregarBicho(bicho);
	}

	@Test
	public void bichoEvolucionaPikachuEnRaichu() {
		entrenador.evolucionar(bicho);
		assertEquals(bicho.getEspecie().getNombre(), "raichu");
	}



	@Test
	public void bichoEvolucionarDosVecesPikachuEnNewchu(){
		entrenador.evolucionar(bicho);
		entrenador.evolucionar(bicho);
		assertEquals(bicho.getEspecie(), newchu);
	}

	
}
