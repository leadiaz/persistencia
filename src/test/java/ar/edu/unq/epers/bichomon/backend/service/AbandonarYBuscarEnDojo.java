package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.NoHayBichosException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionIncorrectaException;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;


public class AbandonarYBuscarEnDojo {

	DataHibernateDao dao;
	BichoServiceImp bichoService;
	EspecieServiceImp especieService;
	DataServiceImp service;
	Bicho bicho1;
	Bicho bicho2;
	@Mock
	FactorBusqueda factorBusqueda;
	
	@Before
	public void setUp() throws Exception {
		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);
		
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());	
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
		
		Dojo dojo = service.recuperar(Dojo.class, 2);
		bicho1 = especieService.crearBicho("pikachu");
		bicho2 = especieService.crearBicho("raichu");
		Entrenador entrenador = service.recuperar(Entrenador.class, 1);
		entrenador.agregarBicho(bicho1);	
		entrenador.agregarBicho(bicho2);
		entrenador.setUbicacionActual(dojo);
		service.update(entrenador);
	}

	@Test(expected = UbicacionIncorrectaException.class)
	public void cuandoUnEntrenadorQuiereAbandonarUnBichoPeroSeEncuentraEnUnDojo() {
		bichoService.abandonar("Juan", 1);	
	}

	@Test(expected = NoHayBichosException.class)
	public void cuandoUnEntrenadorQuiereBuscarUnBichoPeroElDojoNoTieneCampeon(){
		bichoService.buscar("Juan");
	}
	
	@Test
	public void cuandoUnEntrenadorQuiereBuscarUnBichoEnUnDojoConCampeonEspecieRaichuYEncuentraUnPikachu(){
		Bicho bicho3 = especieService.crearBicho("newchu");
		Dojo dojo = service.recuperar(Dojo.class, 2);
		dojo.setBichoCampeon(bicho3);
		service.update(dojo);
		Bicho nuevoBicho = bichoService.buscar("Juan");
		assertEquals(nuevoBicho.getEspecie().getNombre(), "pikachu");
	}
	
	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}
}
