package ar.edu.unq.epers.bichomon.backend.service.especie;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.especie.Especie;
import ar.edu.unq.epers.bichomon.backend.model.especie.TipoBicho;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Guarderia;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;

public class EspecieServiceTest {
	
	EspecieServiceImp especieService;
	DataHibernateDao dao;
	DataServiceImp service;
	Especie raichu; 
	Especie pikachu;
	
	@Before
	public void setUp() throws Exception 
	{				
		dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		especieService = new EspecieServiceImp(dao);
		raichu = new Especie("raichu", null, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0); 
		pikachu = new Especie("pikachu", raichu, null, 10, 10, "foto", TipoBicho.ELECTRICIDAD, 10, 0); 
	}
	
	@Test
	public void testCrearEspecieYGetEspecie() {
		especieService.crearEspecie(raichu);
		Especie especie = especieService.getEspecie("raichu");
		assertEquals(especie.getNombre(), "raichu");
		assertEquals(especie.getAltura(), 10);
		assertEquals(especie.getCantidadBichos(), 0);
		assertEquals(especie.getEnergiaInicial(), 10);
		assertEquals(especie.getPeso(), 10);
	}
	
	@Test
	public void testGetAllEspecies() {
		especieService.crearEspecie(raichu);
		especieService.crearEspecie(pikachu);
		raichu.setRaiz(pikachu);
		pikachu.setRaiz(pikachu);
		especieService.actualizarEspecie(pikachu);
		especieService.actualizarEspecie(raichu);

		List<Especie> especies = especieService.getAllEspecies();
		assertEquals(especies.get(0).getNombre(), "raichu");
		assertEquals(especies.get(0).getRaiz().getNombre(), "pikachu");
		assertEquals(especies.get(1).getNombre(), "pikachu");
		assertEquals(especies.get(1).getEvolucion().getNombre(), "raichu");
	}
	
	@Test
	public void cuandoSeCreaUnNuevoBichoDeUnaEspecieEstaRegistraLaNuevaCantidadDeBichos() {
		especieService.crearEspecie(raichu);
		especieService.crearBicho("raichu");
		
		Bicho bicho = service.recuperar(Bicho.class, 1);
		assertEquals(bicho.getEspecie().getNombre(), "raichu");
		
		
		Especie especie = especieService.getEspecie("raichu");
		assertEquals(especie.getNombre(), "raichu");
		assertEquals(especie.getCantidadBichos(), 1);	
	}	
	
	@Test
	public void testEspeciesPopulares(){
		service.crearSetDatosIniciales();
		Entrenador entrenador = new Entrenador("Red");
		Entrenador entrenador3 = new Entrenador("Misty");
		service.guardar(entrenador);
		service.guardar(entrenador3);
		Bicho bicho1 = especieService.crearBicho("pikachu");
		Bicho bicho2 = especieService.crearBicho("pikachu");
		Bicho bicho3 = especieService.crearBicho("pikachu");
		Bicho bicho4 = especieService.crearBicho("raichu");
		Bicho bicho5 = especieService.crearBicho("raichu");
		Bicho bicho6 = especieService.crearBicho("newchu");
		entrenador.agregarBicho(bicho1);
		entrenador.agregarBicho(bicho4);
		entrenador.agregarBicho(bicho6);
		Entrenador entrenador2 = service.recuperarPorColumna(Entrenador.class, "Juan", "nombre");
		entrenador2.agregarBicho(bicho2);
		entrenador2.agregarBicho(bicho5);
		entrenador3.agregarBicho(bicho3);
		service.update(entrenador2);
		service.update(entrenador3);
		service.update(entrenador);
		List<Especie>populares = especieService.populares();
		assertEquals(populares.size(), 3);
		assertEquals(populares.get(0).getNombre(), "pikachu");
		assertEquals(populares.get(1).getNombre(), "raichu");
		assertEquals(populares.get(2).getNombre(), "newchu");
	}
	
	@Test
	public void testEspeciesImpopulares(){
		service.crearSetDatosIniciales();
		Guarderia guarderia1 = new Guarderia("Guarderia los hdp");
		Guarderia guarderia2 = new Guarderia("Guarderia atendedor de boludos");
		Guarderia guarderia3 = new Guarderia("Guarderia Pepe");
		service.guardar(guarderia1);
		service.guardar(guarderia2);
		service.guardar(guarderia3);
		Bicho bicho1 = especieService.crearBicho("pikachu");
		Bicho bicho2 = especieService.crearBicho("pikachu");
		Bicho bicho3 = especieService.crearBicho("pikachu");
		Bicho bicho4 = especieService.crearBicho("raichu");
		Bicho bicho5 = especieService.crearBicho("raichu");
		Bicho bicho6 = especieService.crearBicho("newchu");
		guarderia1.abandonar(bicho1);
		guarderia1.abandonar(bicho4);
		guarderia1.abandonar(bicho6);
		guarderia2.abandonar(bicho2);
		guarderia3.abandonar(bicho3);
		guarderia3.abandonar(bicho5);
		service.update(guarderia1);
		service.update(guarderia2);
		service.update(guarderia3);
		List<Especie>impopulares = especieService.impopulares();
		assertEquals(impopulares.size(), 3);
		assertEquals(impopulares.get(0).getNombre(), "pikachu");
		assertEquals(impopulares.get(1).getNombre(), "raichu");
		assertEquals(impopulares.get(2).getNombre(), "newchu");
	}
		
	@After
	public void tearDown() throws Exception{	
		dao.eliminarDatos();
	}
}
