package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.BichoNoEncontradoException;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.HistorialCampeon;
import ar.edu.unq.epers.bichomon.backend.model.resultadoCombate.Resultado;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.FactorBusqueda;
import ar.edu.unq.epers.bichomon.backend.service.bichoService.imp.BichoServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;

public class DueloTestCase {
	
	DataHibernateDao dao;
	BichoServiceImp bichoService;
	EspecieServiceImp especieService;
	DataServiceImp service;
	Entrenador ash;
	@Mock
	FactorBusqueda factorBusqueda;
	
	@Before
	public void setUp() throws Exception {
		factorBusqueda = Mockito.mock(FactorBusqueda.class);
		Mockito.when(factorBusqueda.busquedaExitosa()).thenReturn(true);	
		
		DataHibernateDao dao = new DataHibernateDao();
		service = new DataServiceImp(dao);
		service.crearSetDatosIniciales();
		this.dao = new DataHibernateDao();
		bichoService = new BichoServiceImp(dao, new EventoDAO(), new CacheCampeones());
		bichoService.SetFactorBusqueda(factorBusqueda);
		especieService = new EspecieServiceImp(dao);
		Entrenador entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Dojo dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		entrenador.setUbicacionActual(dojo);
		service.update(entrenador);
		
		ash = new Entrenador("Ash");
		service.guardar(ash);
	}

	@Test(expected = BichoNoEncontradoException.class) 
	public void unEntrenadorNoPuedeTenerUnDueloSiNoTieneElBichoElegido(){		
		bichoService.duelo("Juan", 1);
	}
	
	@Test 
	public void cuandoUnEntrenadorRetaAUnDojoSinCampeonSuBichoSeraElNuevoCampeon(){	
		Entrenador entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		service.update(entrenador);
		
		Resultado res = bichoService.duelo("Juan", 1);
		assertEquals(res.getGanador().getEspecie().getNombre(), "pikachu");
		
		Dojo dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		Bicho campeon = dojo.getBichoCampeon();
		
		HistorialCampeon historial = dojo.getHistorialCampeones().get(0);
		
		assertEquals(campeon.getId(), 1);
		assertEquals(historial.getBichoCampeon().getId(), 1);
	}
	
	@Test 
	public void cuandoUnEntrenadorGanaElDueloSuBichoSeraElNuevoCampeon(){	
		Entrenador entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		Bicho bicho = especieService.crearBicho("pikachu");
		entrenador.agregarBicho(bicho);
		service.update(entrenador);
		bichoService.duelo("Juan", 1);
		Dojo dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		ash.setUbicacionActual(dojo);
		Bicho raichu = especieService.crearBicho("raichu");
		ash.agregarBicho(raichu);
		service.update(ash);
		
		Resultado res = bichoService.duelo("Ash", 2);
		assertEquals(res.getGanador().getEspecie().getNombre(), "raichu");
		assertEquals(res.getPerdedor().getEspecie().getNombre(), "pikachu");
	}
	
	@After
	public void cleanup() {
		this.dao.eliminarDatos();
	}		
}
