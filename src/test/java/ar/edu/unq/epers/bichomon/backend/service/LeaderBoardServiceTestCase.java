package ar.edu.unq.epers.bichomon.backend.service;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.bicho.Bicho;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.entrenador.HistorialCampeon;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.especie.impl.EspecieServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.leaderboardService.impl.LeaderboardServiceImpl;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCampeones;
import ar.edu.unq.epers.bichomon.backend.service.runner.SessionFactoryProvider;

public class LeaderBoardServiceTestCase {
	DataServiceImp service;
	LeaderboardServiceImpl leader;
	EspecieServiceImp especieService;
	DataHibernateDao dao;
	Entrenador ash;
	Dojo alpha ;
	HistorialCampeon histCampeon1;
	HistorialCampeon histCampeon2DojoAlpha;
	Entrenador entrenador;
	Dojo dojo;
	CacheCampeones cache;
	@Before
	public void setUp() throws Exception{
		dao = new DataHibernateDao();
		service = new DataServiceImp(new DataHibernateDao());
		service.crearSetDatosIniciales();
		cache = new CacheCampeones();
		leader = new LeaderboardServiceImpl(dao, cache);
		especieService = new EspecieServiceImp(dao);
		ash = new Entrenador("Ash");
		service.guardar(ash);
		alpha = new Dojo("Alpha");
		service.guardar(alpha);
		cache.clear();
		
		
		entrenador = service.recuperarPorColumna(Entrenador.class, "juan", "nombre");
		dojo = service.recuperarPorColumna(Dojo.class, "Cobra", "nombreUbicacion");
		Bicho bichoCamp1 = especieService.crearBicho("pikachu");
		Bicho bichoCamp2 = especieService.crearBicho("pikachu");
		Bicho bichoCamp3 = especieService.crearBicho("pikachu");
		this.ash.agregarBicho(bichoCamp1);
		this.ash.agregarBicho(bichoCamp2);
		entrenador.agregarBicho(bichoCamp3);
		histCampeon1 = new HistorialCampeon(bichoCamp3,dojo);
		histCampeon2DojoAlpha = new HistorialCampeon(bichoCamp2,this.alpha);
	    histCampeon1.setFechaInicio(new Date(2017,4,10));
	    histCampeon2DojoAlpha.setFechaInicio(new Date(2017,5,10));
	    dojo.getHistorialCampeones().add(histCampeon1);
	    dojo.setBichoCampeon(bichoCamp3);
	    alpha.getHistorialCampeones().add(histCampeon2DojoAlpha);
	    alpha.setBichoCampeon(bichoCamp2);
	    service.update(dojo);
	    service.update(alpha);
		service.update(ash);
		service.update(entrenador);
	}
	
	@Test
	public void TestObtenerLosEntrenadoresCampeonesDelMomento(){	
		assertEquals("Juan",leader.campeones().get(0).getNombre());
		assertEquals("Ash",leader.campeones().get(1).getNombre());
		// ahora cambiamos las fechas por lo que deberia cambiar el orden en el que estan en la lista
		histCampeon2DojoAlpha.setFechaInicio(new Date(2017,4,10));
		histCampeon1.setFechaInicio(new Date(2017,5,10));
	    service.update(dojo);
	    service.update(alpha);
	    assertEquals("Ash",leader.campeones().get(0).getNombre());
		assertEquals("Juan",leader.campeones().get(1).getNombre());
	}
	
	@Test
	public void TestObtenerLaEspecieLiderDeTodosLosDojos(){
	    assertEquals("pikachu",leader.especieLider().getNombre());
	    //ahora provamos poniendo 4 raichus 
		Bicho bichoRaichuCamp1 = especieService.crearBicho("raichu");
		Bicho bichoRaichuCamp2 = especieService.crearBicho("raichu");
		Bicho bichoRaichuCamp3 = especieService.crearBicho("raichu");
		Bicho bichoRaichuCamp4 = especieService.crearBicho("raichu");
		this.ash.agregarBicho(bichoRaichuCamp1);
		this.ash.agregarBicho(bichoRaichuCamp2);
		entrenador.agregarBicho(bichoRaichuCamp3);
		entrenador.agregarBicho(bichoRaichuCamp4);
		HistorialCampeon histCampeonRaichu = new HistorialCampeon(bichoRaichuCamp1,dojo);
		HistorialCampeon histCampeonRaichu2 = new HistorialCampeon(bichoRaichuCamp2,dojo);
		HistorialCampeon histCampeonRaichuDojoAlpha = new HistorialCampeon(bichoRaichuCamp3,this.alpha);
		HistorialCampeon histCampeonRaichuDojoAlpha2 = new HistorialCampeon(bichoRaichuCamp4,this.alpha);
		dojo.getHistorialCampeones().add(histCampeonRaichu);
		dojo.getHistorialCampeones().add(histCampeonRaichu2);
		alpha.getHistorialCampeones().add(histCampeonRaichuDojoAlpha);
		alpha.getHistorialCampeones().add(histCampeonRaichuDojoAlpha2);
	    service.update(dojo);
	    service.update(alpha);
		service.update(ash);
		service.update(entrenador);
		assertEquals("raichu",leader.especieLider().getNombre());
	}
	
	@Test
	public void TestObtenerLosEntrenadoresConMayorPoderCombinadoDeSusBichos(){
	    assertEquals("Ash",leader.lideres().get(0).getNombre());
		assertEquals("Juan",leader.lideres().get(1).getNombre());
		// retorna primero a ash ya que tiene dos pichachus y juan solo uno por lo tanto
		// ash le gana en poder a juansito 
	}
	@After
	public void cleanup() {
//		Destroy cierra la session factory y fuerza a que, la proxima vez, una nueva tenga
//		que ser creada.
//		
//		Al tener hibernate configurado con esto <property name="hibernate.hbm2ddl.auto">create-drop</property>
//		al crearse una nueva session factory todo el schema será destruido y creado desde cero.
		SessionFactoryProvider.destroy();
	}
	
}