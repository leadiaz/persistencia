package ar.edu.unq.epers.bichomon.backend.service.mapa;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.epers.bichomon.backend.model.entrenador.Entrenador;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.CaminoMuyCostosoException;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Dojo;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.Ubicacion;
import ar.edu.unq.epers.bichomon.backend.model.ubicacion.UbicacionMuyLejanaException;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataHibernateDao;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.data.impl.DataServiceMapa;
import ar.edu.unq.epers.bichomon.backend.service.evento.EventoDAO;
import ar.edu.unq.epers.bichomon.backend.service.mapaService.impl.MapaServiceImp;
import ar.edu.unq.epers.bichomon.backend.service.redis.CacheCantEntrenadores;
import ar.edu.unq.epers.bichomon.backend.service.ubicacion.UbicacionDao;

public class MapaServiceNeo4jTest {

	DataServiceMapa service;
	MapaServiceImp mapaService;
	UbicacionDao ubicacionDao;
	DataHibernateDao hibernateDao;
	DataServiceImp serviceData;
	@Before
	public void setUp() throws Exception {
		hibernateDao = new DataHibernateDao();
		serviceData = new DataServiceImp(hibernateDao);
		ubicacionDao = new UbicacionDao();
		mapaService = new MapaServiceImp(hibernateDao, new UbicacionDao(), new EventoDAO(), new CacheCantEntrenadores());
		service = new DataServiceMapa(mapaService);	
		service.crearSetDatosIniciales();
	}
	
	
	@Test
	public void crearUbicacionTest() {
		ubicacionDao.eleminarDatos();
		serviceData.eliminarDatos();
		Dojo dojo = new Dojo(1, "Cobra");
		mapaService.crearUbicacion(dojo);
		
		Ubicacion dojoGuardado = serviceData.recuperar(Dojo.class, 1);
		Map<String, Integer> dojoMapa = ubicacionDao.getUbicacion(1);
		
		assertEquals(dojoGuardado.getNombreUbicacion(), "Cobra");
		assertTrue(dojoMapa.containsKey("Cobra"));
		service.crearSetDatosIniciales();
		
	}

	@Test
	public void conectadosTest() {
		List<Ubicacion> ubicaciones = mapaService.conectados("Tibet Dojo", "terrestre");
		assertEquals(ubicaciones.size(), 1);
		assertEquals(ubicaciones.get(0).getNombreUbicacion(), "Plantaland");
	}

	@Test(expected = UbicacionMuyLejanaException.class)
	public void moverAUbicacionLejanaTest() {	
		mapaService.mover("ash", "Bicholand");
	}
	
	@Test
	public void moverTest() {	
		mapaService.mover("ash", "Agualand");
		Entrenador ash = this.serviceData.recuperarPorColumna(Entrenador.class, "ash", "nombre");
		assertEquals(ash.getUbicacionActual().getNombreUbicacion(), "Agualand");
		assertEquals(ash.getMonedas(), 98);
	}
	
	@Test(expected = CaminoMuyCostosoException.class)
	public void moverPorCaminoMuyCostosoTest() {
		Entrenador e = serviceData.recuperarPorColumna(Entrenador.class, "ash", "nombre");
		e.setMonedas(1);
		serviceData.update(e);
		mapaService.mover("ash", "Agualand");
	}
	
	@Test
	public void moverMasCortoTest() {	
		mapaService.moverMasCorto("ash", "Bicholand");
		Entrenador ash = this.serviceData.recuperarPorColumna(Entrenador.class, "ash", "nombre");
		assertEquals(ash.getUbicacionActual().getNombreUbicacion(), "Bicholand");
		assertEquals(ash.getMonedas(), 96);
	}
	
	@Test(expected = CaminoMuyCostosoException.class)
	public void moverMasCortoPorCaminoMuyCostosoTest() {
		Entrenador e = serviceData.recuperarPorColumna(Entrenador.class, "ash", "nombre");
		e.setMonedas(1);
		serviceData.update(e);	
		mapaService.moverMasCorto("ash", "Bicholand");
	}
	
	@After
	public void cleanup() {
		ubicacionDao.eleminarDatos();
		serviceData.eliminarDatos();
	}
}
